﻿namespace Interface
{
    partial class ScanFolderDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelText = new System.Windows.Forms.Label();
            this.progressBarScanFolder = new System.Windows.Forms.ProgressBar();
            this.labelStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(13, 13);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(344, 13);
            this.labelText.TabIndex = 0;
            this.labelText.Text = "Finding TV episodes and downloading details. This may take a minute...";
            this.labelText.UseWaitCursor = true;
            // 
            // progressBarScanFolder
            // 
            this.progressBarScanFolder.Location = new System.Drawing.Point(81, 41);
            this.progressBarScanFolder.Name = "progressBarScanFolder";
            this.progressBarScanFolder.Size = new System.Drawing.Size(200, 23);
            this.progressBarScanFolder.TabIndex = 1;
            // 
            // labelStatus
            // 
            this.labelStatus.Location = new System.Drawing.Point(12, 77);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(339, 15);
            this.labelStatus.TabIndex = 2;
            this.labelStatus.Text = "<status>";
            this.labelStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ScanFolderDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(363, 101);
            this.Controls.Add(this.labelStatus);
            this.Controls.Add(this.progressBarScanFolder);
            this.Controls.Add(this.labelText);
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ScanFolderDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Scanning Folder...";
            this.UseWaitCursor = true;
            this.Load += new System.EventHandler(this.ScanFolderDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.ProgressBar progressBarScanFolder;
        private System.Windows.Forms.Label labelStatus;
    }
}