﻿namespace Interface
{
    partial class FileOrganizationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelText = new System.Windows.Forms.Label();
            this.progressBarFileOrganization = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // labelText
            // 
            this.labelText.AutoSize = true;
            this.labelText.Location = new System.Drawing.Point(13, 13);
            this.labelText.Name = "labelText";
            this.labelText.Size = new System.Drawing.Size(250, 13);
            this.labelText.TabIndex = 0;
            this.labelText.Text = "Organizing your files. This may take a few minutes...";
            this.labelText.UseWaitCursor = true;
            // 
            // progressBarFileOrganization
            // 
            this.progressBarFileOrganization.Location = new System.Drawing.Point(41, 38);
            this.progressBarFileOrganization.Name = "progressBarFileOrganization";
            this.progressBarFileOrganization.Size = new System.Drawing.Size(189, 23);
            this.progressBarFileOrganization.TabIndex = 1;
            this.progressBarFileOrganization.UseWaitCursor = true;
            // 
            // FileOrganizationDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 83);
            this.Controls.Add(this.progressBarFileOrganization);
            this.Controls.Add(this.labelText);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FileOrganizationDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Organizing files...";
            this.UseWaitCursor = true;
            this.Load += new System.EventHandler(this.FileOrganizationDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelText;
        private System.Windows.Forms.ProgressBar progressBarFileOrganization;
    }
}