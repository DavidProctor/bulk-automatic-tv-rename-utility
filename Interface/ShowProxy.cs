﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Batru;

namespace Interface
{
    /// <summary>
    /// Represents a single ShowNode in Interface.
    /// </summary>
    public class ShowProxy
    {
        public static List<ShowProxy> allEpisodeProxies;
        public static List<ShowProxy> allDirectoryProxies;

        private ShowNode _original; // Reference to the original ShowNode this Proxy stands in for
        private List<ShowProxy> _children;
        private string _latestSeriesTitle;

        // Special season number values that indicate something other than a number
        public static Dictionary<int, string> seasonReplacements = new Dictionary<int, string>()
        {
            { 0, "[Special]" },
            { -1, "[Miniseries]" },
            { -2, "[Unknown]" },
            { -3, "[Mixed]" }
        };

        // Special episode number values that indicate something other than a number
        public static Dictionary<int, string> episodeReplacements = new Dictionary<int, string>()
        {
            { 0, "[Unknown]" },
            { -1, "[Mixed]" }
        };

        /// <summary>
        /// Constructor for the initial creation of the ShowNode composite pattern.
        /// </summary>
        /// <param name="path">The ShowNode this ShowProxy represents</param>
        public ShowProxy(string path)
        {
            allEpisodeProxies = new List<ShowProxy>();
            allDirectoryProxies = new List<ShowProxy>();
            allDirectoryProxies.Add(this);

            _original = new Directory(path);
            _children = this.GetChildProxies();
        }

        /// <summary>
        /// Private constructor called by ShowProxy to create children
        /// </summary>
        /// <param name="original">The ShowNode this ShowProxy represents</param>
        private ShowProxy(ShowNode original)
        {
            _original = original;

            if (this.HasChildren)
            {
                allDirectoryProxies.Add(this);
            }
            else
            {
                allEpisodeProxies.Add(this);
            }

            _children = this.GetChildProxies();
        }

        /// <summary>
        /// Returs a list of ChildProxies representing the children of _original. Returns null if the original is not a Directory.
        /// </summary>
        /// <returns>A list of ChildProxies representing the children of _original, or null if the original is not a Directory.</returns>
        private List<ShowProxy> GetChildProxies()
        {
            if (_original is Directory)
            {
                Directory dir = _original as Directory;
                List<ShowProxy> childProxies = new List<ShowProxy>();

                foreach (ShowNode node in dir.contents)
                {
                    childProxies.Add(new ShowProxy(node));
                }
                return childProxies;
            }
            return null;
        }

        public void GetAllChildProxies(out List<ShowProxy> allChildEpisodes, out List<ShowProxy> allChildDirectories)
        {
            allChildEpisodes = new List<ShowProxy>();
            allChildDirectories = new List<ShowProxy>();
            if (this.HasChildren)
            {
                foreach (ShowProxy thisChild in this.GetChildProxies())
                {
                    if (thisChild.HasChildren)
                    {
                        List<ShowProxy> thisChildsEpisodes;
                        List<ShowProxy> thisChildsDirectories;
                        thisChild.GetAllChildProxies(out thisChildsEpisodes, out thisChildsDirectories);
                        allChildDirectories.AddRange(thisChildsDirectories);
                        allChildEpisodes.AddRange(thisChildsEpisodes);
                        allChildDirectories.Add(this);
                    }
                    else
                    {
                        allChildEpisodes.Add(this);
                    }
                }
            }
            else
            {
                allChildEpisodes.Add(this);
            }
        }

        /// <summary>
        /// Calls Batru.SeriesAnalyzer on this ShowProxy.
        /// </summary>
        public void Download()
        {
            _original.Accept(SeriesAnalyzer.GetInstance());
        }

        /// <summary>
        /// Organizes the associated ShowNode according to the current FileOrganizationSettings.
        /// </summary>
        public void Organize()
        {
            FileOrganizationSettings settings = BatruFacade.GetOrganizationSettings();
            // Fail if the settings are not set up
            if (settings == null)
            {
                throw new ArgumentNullException("FileOrganizationSettings must be defined before organizing files.");
            }

            // Organize the associated Directory
            if (this.HasChildren)
            {
                FileOrganizer.OrganizeDirectory(_original as Directory, settings);
            }
            // Organize the associated Episode
            else
            {
                FileOrganizer.OrganizeEpisode(_original as Episode, settings);
            }
        }

        /// <summary>
        /// Calls the FilenameAnalyzer visitor on the associated ShowNode.
        /// </summary>
        public void AnalyzeFilename()
        {
            _original.Accept(FilenameAnalyzer.GetInstance());
        }

        /// <summary>
        /// Calls the ContextAnalyzer visitor on the associated ShowNode.
        /// </summary>
        public void AnalyzeContext()
        {
            _original.Accept(ContextAnalyzer.GetInstance());
        }

        /// <summary>
        /// Checks itself on all children, then sets _latestSeriesTitle to the current value of _original.LikeliestSeriesTitle
        /// </summary>
        public void CheckSeriesTitleWithChildren()
        {
            if (this.HasChildren)
            {
                foreach (ShowProxy child in _children)
                {
                    child.CheckSeriesTitleWithChildren();
                }
            }

            _latestSeriesTitle = _original.LikeliestSeriesTitle;
        }

        /// <summary>
        /// The associated ShowNode's filename
        /// </summary>
        public string Filename
        {
            get
            {
                return _original.Filename;
            }
        }

        /// <summary>
        /// The associated ShowNode's likeliest series title
        /// </summary>
        public string SeriesTitle
        {
            get
            {
                // Episodes are simple
                if (!this.HasChildren)
                {
                    return _latestSeriesTitle;
                }
                // For directories, return consistent title of children or "[Mixed]"
                else
                {
                    string consistentSeries = string.Empty;
                    foreach (ShowProxy child in _children)
                    {
                        if (string.IsNullOrEmpty(consistentSeries))
                        {
                            consistentSeries = child.SeriesTitle;
                        }
                        else if (consistentSeries != child.SeriesTitle)
                        {
                            consistentSeries = "[Mixed]";
                            break;
                        }
                    }
                    return consistentSeries;
                }
            }
            set
            {
                if (_original.LikeliestSeries == null || value != _original.LikeliestSeries.Title)
                {
                    _original.AddSeriesCandidate(value, Certainty.UserSupplied);
                }
                this.CheckSeriesTitleWithChildren();
            }
        }

        /// <summary>
        /// The associated ShowNode's likeliest season number, in string format
        /// </summary>
        public string Season
        {
            get
            {
                int season = _original.SeasonNum;
                string replacement;
                if (seasonReplacements.TryGetValue(season, out replacement))
                {
                    return replacement;
                }
                return season.ToString();
            }
        }

        /// <summary>
        /// The season number of the associated ShowNode.
        /// </summary>
        public int SeasonNum
        {
            get
            {
                return _original.SeasonNum;
            }
            set
            {
                _original.SeasonNum = value;
            }
        }

        /// <summary>
        /// The episode number within the season of the associated ShowNode.
        /// </summary>
        public int EpisodeNum
        {
            get
            {
                return _original.EpisodeNum;
            }
            set
            {
                _original.EpisodeNum = value;
            }
        }

        /// <summary>
        /// The associated ShowNode's likeliest episode number, in string format
        /// </summary>
        public string Episode
        {
            get
            {
                int episode = _original.EpisodeNum;
                string replacement;
                if (episodeReplacements.TryGetValue(episode, out replacement))
                {
                    return replacement;
                }
                return episode.ToString();
            }
        }

        /// <summary>
        /// The likeliest episode title of the associated ShowNode.
        /// </summary>
        public string EpisodeTitle
        {
            get
            {
                return _original.EpisodeTitle;
            }
            set
            {
                _original.SetEpisodeTitle(value, Certainty.UserSupplied);
            }
        }

        /// <summary>
        /// The associated ShowNode's debug string
        /// </summary>
        public string Debug
        {
            get
            {
                return _original.Debug;
            }
        }

        /// <summary>
        /// A list of proxies representing the associated ShowNode's children. Empty if the associated ShowNode is not a Directory.
        /// </summary>
        public List<ShowProxy> Children
        {
            get
            {
                return _children;
            }
        }

        /// <summary>
        /// Whether the associated ShowNode has any children.
        /// </summary>
        public bool HasChildren
        {
            get
            {
                return _children != null;
            }
        }

        /// <summary>
        /// Returns all of the associated ShowNode's series candidates.
        /// </summary>
        public Dictionary<Series, Certainty> SeriesCandidates
        {
            get
            {
                return _original.SeriesCandidates;
            }
        }

        /// <summary>
        /// Returns a List of all Series candidates and all of their alternatives for the associated ShowNode.
        /// </summary>
        public List<string> seriesCandidateList
        {
            get
            {
                List<string> seriesList = new List<string>();
                foreach (KeyValuePair<Series, Certainty> candidate in _original.SeriesCandidates)
                {
                    if (candidate.Key._alternatives != null)
                    {
                        foreach (Series alternate in candidate.Key._alternatives)
                        {
                            if (!seriesList.Contains(alternate.Title))
                            {
                                seriesList.Add(alternate.Title);
                            }
                        }
                    }
                    if (!seriesList.Contains(candidate.Key.Title))
                    {
                        seriesList.Add(candidate.Key.Title);
                    }
                }
                return seriesList;
            }
        }

        /// <summary>
        /// Given formatting details, returns a formatted filename for this ShowProxy.
        /// </summary>
        /// <param name="format">The general format to enforce for the filename.</param>
        /// <param name="identifierStyle">The format to enforce for the season/episode identifier.</param>
        /// <param name="removeYear">Whether to remove year suffixes from the series title.</param>
        /// <returns>A formatted filename for this ShowProxy.</returns>
        public string GetFormattedFilename(string format, string identifierStyle, bool removeYear)
        {
            if (this.HasChildren)
            {
                return string.Empty;
            }
            else if (this.SeasonNum >= -1 && this.EpisodeNum > 0 && this.seriesCandidateList.Count > 0)
            {
                return FileOrganizer.GetFormattedFilename(_original as Episode, format, identifierStyle, removeYear);
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
