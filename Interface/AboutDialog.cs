﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Reflection;
using System.Diagnostics;

namespace Interface
{
    /// <summary>
    /// The About information for the program.
    /// </summary>
    public partial class AboutDialog : Form
    {
        /// <summary>
        /// Default Form constructor.
        /// </summary>
        public AboutDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Sets the version number in labelVersion.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void AboutDialog_Load(object sender, EventArgs e)
        {
            Assembly assembly = Assembly.GetExecutingAssembly();
            FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fileVersionInfo.ProductVersion;

            labelVersion.Text = string.Format(labelVersion.Text, version);
        }

        /// <summary>
        /// Links to DavidProctor.ca.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void linkLabelDavidProctor_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.davidproctor.ca/");
        }

        /// <summary>
        /// Links to the BATRU homepage on DavidProctor.ca.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void linkLabelHomepage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://davidproctor.ca/?p=86");
        }

        /// <summary>
        /// Links to the source code on BitBucket.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void linkLabelSource_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://bitbucket.org/Taepodong/bulk-automatic-tv-rename-utility");
        }

        /// <summary>
        /// Opens the license in a text editor.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void linkLabelLicense_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LicenseForm dlg = new LicenseForm();
            dlg.Show();
        }

        /// <summary>
        /// Links to the donations page.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void linkLabelDonate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://davidproctor.ca/?p=86#donate");
        }

        /// <summary>
        /// Closes the dialog.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Links to the icon on the web.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void linkLabelIcon_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.iconarchive.com/show/pry-frente-black-special-icons-by-binassmax/tv-2-icon.html");
        }

        /// <summary>
        /// Links to ObjectListView on the web.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void linkLabelObjectListView_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://objectlistview.sourceforge.net/cs/index.html");
        }

        /// <summary>
        /// Links to TheTVDB.com.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void linkLabelTVDB_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.thetvdb.com/");
        }
    }
}
