﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface
{
    /// <summary>
    /// A NumericUpDown control that substitutes replacement values for numbers.
    /// </summary>
    public partial class NumericUpDownOptionalText : NumericUpDown
    {
        /// <summary>
        /// Default form control constructor.
        /// </summary>
        public NumericUpDownOptionalText()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Substitutes a number with a replacement string.
        /// </summary>
        protected override void UpdateEditText()
        {
            int setting = Convert.ToInt32(this.Value);
            string textValue;

            if (Replacements != null && Replacements.TryGetValue(setting, out textValue))
            {
                this.Text = textValue;
            }
            else
            {
                this.Text = this.Value.ToString();
            }
        }

        /// <summary>
        /// The dictionary of replacements to enforce.
        /// </summary>
        public Dictionary<int, string> Replacements
        {
            get;
            set;
        }

        /// <summary>
        /// Instantaneously update the current Text of the control.
        /// </summary>
        public void ForceReplacements()
        {
            this.UpdateEditText();
        }
    }
}
