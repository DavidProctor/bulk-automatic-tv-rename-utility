﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface
{
    /// <summary>
    /// A non-interactive form that shows an animation while TV episodes are searched for, identified, and downloaded (if not in offline mode).
    /// </summary>
    public partial class ScanFolderDialog : Form
    {
        private BackgroundWorker _worker;
        private List<ShowProxy> _failedDownloads;

        /// <summary>
        /// Constructor for this Form.
        /// </summary>
        public ScanFolderDialog()
        {
            InitializeComponent();
            _failedDownloads = new List<ShowProxy>();
            labelStatus.Text = string.Empty;
        }

        /// <summary>
        /// The path to the root folder to search for TV episodes. Passed to this dialog before it is shown.
        /// </summary>
        public string RootPath
        {
            get;
            set;
        }

        /// <summary>
        /// The root ShowProxy representing the origin ShowNode composite that contains all ShowNodes. Returned after execution.
        /// </summary>
        public ShowProxy Root
        {
            get;
            set;
        }

        /// <summary>
        /// Creates a BackgroundWorker that identifies ShowNodes and populates them.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void ScanFolderDialog_Load(object sender, EventArgs e)
        {
            _worker = new BackgroundWorker();
            _worker.DoWork += worker_DoWork;
            _worker.ProgressChanged += worker_ProgressChanged;
            _worker.WorkerReportsProgress = true;
            _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            _worker.RunWorkerAsync();
        }

        /// <summary>
        /// BackgroundWorker delegate that identifies and, if we're not in offline mode, downloads details in the folder pointed to by RootPath.
        /// </summary>
        /// <param name="sender">Default delegate parameter.</param>
        /// <param name="e">Default delegate parameter.</param>
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            bool performOfflineAnalysis = true;
            List<ShowProxy> episodesToAnalyze = null;
            List<ShowProxy> directoriesToAnalyze = null;

            // If passed a string path, analyze all recorded nodes
            if (Root == null)
            {
                Root = new ShowProxy(RootPath);
                episodesToAnalyze = ShowProxy.allEpisodeProxies;
                directoriesToAnalyze = ShowProxy.allDirectoryProxies;
            }
            else
            {
                performOfflineAnalysis = false;
                Root.GetAllChildProxies(out episodesToAnalyze, out directoriesToAnalyze);
            }

            // Get total counts for the progress bar
            int totalEpisodes = episodesToAnalyze.Count;
            int totalNodes = totalEpisodes + directoriesToAnalyze.Count;

            // Scan for files and identify using filenames and context
            if (performOfflineAnalysis)
            {
                for (int i = 0; i < totalEpisodes; i++)
                {
                    episodesToAnalyze[i].AnalyzeFilename();
                    int percentage = (i + 1) * 100 / totalNodes;
                    _worker.ReportProgress(percentage, string.Format("Checking object {0} of {1}", i + 1, totalNodes));
                }

                for (int i = 0; i < directoriesToAnalyze.Count; i++)
                {
                    directoriesToAnalyze[i].AnalyzeFilename();
                    directoriesToAnalyze[i].AnalyzeContext();
                    int number = totalEpisodes + i + 1;
                    int percentage = number * 100 / totalNodes;
                    _worker.ReportProgress(percentage, string.Format("Checking object {0} of {1}", number, totalNodes));
                }
            }

            // Connect to TheTVDB and download extra details about selected shows
            if (!BatruFacade.offlineMode)
            {
                int xmlAttempts = 0;

                for (int i = 0; i < totalEpisodes; i++)
                {
                    try
                    {
                        episodesToAnalyze[i].Download();
                        int percentage = (i + 1) * 100 / totalEpisodes;
                        _worker.ReportProgress(percentage, string.Format("Downloading extra details for object {0} of {1}", i + 1, totalEpisodes));
                    }
                    catch (System.Xml.XmlException)
                    {
                        if (xmlAttempts < 3)
                        {
                            xmlAttempts++;
                            i--;
                            continue;
                        }
                        else
                        {
                            _failedDownloads.Add(episodesToAnalyze[i]);
                        }
                    }
                    catch (System.Net.WebException)
                    {
                        _failedDownloads.Add(episodesToAnalyze[i]);
                    }
                }

                //for (int i = 0; i < directoriesToAnalyze.Count; i++)
                //{
                //    try
                //    {
                //        directoriesToAnalyze[i].Download();
                //        int number = totalEpisodes + i + 1;
                //        int percentage = number * 100 / totalNodes;
                //        _worker.ReportProgress(percentage, string.Format("Downloading extra details for object {0} of {1}", number, totalNodes));
                //    }
                //    catch (System.Net.WebException)
                //    {
                //        _failedDownloads.Add(directoriesToAnalyze[i]);
                //    }
                //}
            }

            // Refresh the visible data in ShowProxies
            Root.CheckSeriesTitleWithChildren();
        }

        /// <summary>
        /// Updates the status label and progress bar.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            string label = e.UserState as string;
            labelStatus.Text = label;
            progressBarScanFolder.Value = e.ProgressPercentage;
        }

        /// <summary>
        /// BackgroundWorker delegate that closes this dialog upon successful completion of file scan.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (_failedDownloads.Count > 0)
            {
                MessageBox.Show(
                    string.Format("Failed to download details for {0} episodes. Please check your internet connection and try again.", _failedDownloads.Count),
                    "Download Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
            }
            this.DialogResult = DialogResult.OK;
        }
    }
}
