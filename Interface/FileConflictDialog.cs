﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface
{
    /// <summary>
    /// A dialog that asks the user if they would like to overwrite a conflicting file or skip that file's organization.
    /// </summary>
    public partial class FileConflictDialog : Form
    {
        /// <summary>
        /// Standard Form constructor.
        /// </summary>
        public FileConflictDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Populates labelConflict.
        /// </summary>
        /// <param name="sender">Standard event parameter.</param>
        /// <param name="e">Standard event parameter.</param>
        private void FileConflictDialog_Load(object sender, EventArgs e)
        {
            labelConflict.Text = string.Format(labelConflict.Text, Filename, Directory);
        }

        /// <summary>
        /// Closes the dialog and indicates that the file should be overwritten.
        /// </summary>
        /// <param name="sender">Standard event parameter.</param>
        /// <param name="e">Standard event parameter.</param>
        private void buttonOverwrite_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }

        /// <summary>
        /// Closes the dialog and indicates that the file should be skipped.
        /// </summary>
        /// <param name="sender">Standard event parameter.</param>
        /// <param name="e">Standard event parameter.</param>
        private void buttonSkip_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

        /// <summary>
        /// Sets RepeatAction based on the state of checkBoxAllFiles.
        /// </summary>
        /// <param name="sender">Standard event parameter.</param>
        /// <param name="e">Standard event parameter.</param>
        private void checkBoxAllFiles_CheckedChanged(object sender, EventArgs e)
        {
            RepeatAction = checkBoxAllFiles.Checked;
        }

        /// <summary>
        /// The filename that is causing the conflict.
        /// </summary>
        public string Filename
        {
            get;
            set;
        }

        /// <summary>
        /// The string path to the directory that the conflict is occurring in.
        /// </summary>
        public string Directory
        {
            get;
            set;
        }

        /// <summary>
        /// Whether the parent form should repeat the chosen action instead of generating this window again.
        /// </summary>
        public bool RepeatAction
        {
            get;
            set;
        }
    }
}
