﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
 * =====  TO DO =====
 * 
 * --- after release? ---
 * Language settings
 * Analyze ID3 tags
 * Automatically don't create folder for only one season
 * Add FileSystemWatcher to treeListViewBrowser
 * Identify episodes based on "0101"-type patterns
 * Identify episodes based on numeric sequences within a folder
 * 
 * =====   BUGS ======
 * Selection box appears after calling e.Cancel in MainForm
 * 
 * ===== CHANGES =====
 * AboutDialog draws version number from assembly. Fixed MainForm tab order. Setup now produces single file.
 */
namespace Interface
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }
}
