﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface
{
    /// <summary>
    /// Shows the license agreement to the user.
    /// </summary>
    public partial class LicenseForm : Form
    {
        /// <summary>
        /// Default Form constructor.
        /// </summary>
        public LicenseForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Populates the license window with text.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void LicenseForm_Load(object sender, EventArgs e)
        {
            textBoxLicense.Text = Properties.Resources.License;
            buttonClose.Select();
        }

        /// <summary>
        /// Closes the form.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void buttonClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
