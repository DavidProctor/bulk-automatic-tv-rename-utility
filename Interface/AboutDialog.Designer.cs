﻿namespace Interface
{
    partial class AboutDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxIcon = new System.Windows.Forms.PictureBox();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelCredits = new System.Windows.Forms.Label();
            this.linkLabelDavidProctor = new System.Windows.Forms.LinkLabel();
            this.linkLabelHomepage = new System.Windows.Forms.LinkLabel();
            this.linkLabelSource = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabelLicense = new System.Windows.Forms.LinkLabel();
            this.labelDonate = new System.Windows.Forms.Label();
            this.linkLabelDonate = new System.Windows.Forms.LinkLabel();
            this.buttonClose = new System.Windows.Forms.Button();
            this.labelExtraCredits = new System.Windows.Forms.Label();
            this.linkLabelObjectListView = new System.Windows.Forms.LinkLabel();
            this.linkLabelIcon = new System.Windows.Forms.LinkLabel();
            this.linkLabelTVDB = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIcon)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBoxIcon
            // 
            this.pictureBoxIcon.Image = global::Interface.Properties.Resources.icon;
            this.pictureBoxIcon.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxIcon.Name = "pictureBoxIcon";
            this.pictureBoxIcon.Size = new System.Drawing.Size(72, 72);
            this.pictureBoxIcon.TabIndex = 0;
            this.pictureBoxIcon.TabStop = false;
            // 
            // labelTitle
            // 
            this.labelTitle.Font = new System.Drawing.Font("Microsoft JhengHei UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(93, 16);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(190, 60);
            this.labelTitle.TabIndex = 1;
            this.labelTitle.Text = "Bulk Automatic TV Rename Utility";
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(95, 69);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(141, 13);
            this.labelVersion.TabIndex = 2;
            this.labelVersion.Text = "Version {0} / Copyright 2014";
            // 
            // labelCredits
            // 
            this.labelCredits.AutoSize = true;
            this.labelCredits.Location = new System.Drawing.Point(82, 108);
            this.labelCredits.Name = "labelCredits";
            this.labelCredits.Size = new System.Drawing.Size(58, 13);
            this.labelCredits.TabIndex = 3;
            this.labelCredits.Text = "Created by";
            // 
            // linkLabelDavidProctor
            // 
            this.linkLabelDavidProctor.AutoSize = true;
            this.linkLabelDavidProctor.Location = new System.Drawing.Point(135, 108);
            this.linkLabelDavidProctor.Name = "linkLabelDavidProctor";
            this.linkLabelDavidProctor.Size = new System.Drawing.Size(72, 13);
            this.linkLabelDavidProctor.TabIndex = 4;
            this.linkLabelDavidProctor.TabStop = true;
            this.linkLabelDavidProctor.Text = "David Proctor";
            this.linkLabelDavidProctor.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelDavidProctor_LinkClicked);
            // 
            // linkLabelHomepage
            // 
            this.linkLabelHomepage.Location = new System.Drawing.Point(12, 123);
            this.linkLabelHomepage.Name = "linkLabelHomepage";
            this.linkLabelHomepage.Size = new System.Drawing.Size(266, 15);
            this.linkLabelHomepage.TabIndex = 5;
            this.linkLabelHomepage.TabStop = true;
            this.linkLabelHomepage.Text = "BATRU Homepage";
            this.linkLabelHomepage.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.linkLabelHomepage.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelHomepage_LinkClicked);
            // 
            // linkLabelSource
            // 
            this.linkLabelSource.Location = new System.Drawing.Point(12, 138);
            this.linkLabelSource.Name = "linkLabelSource";
            this.linkLabelSource.Size = new System.Drawing.Size(266, 15);
            this.linkLabelSource.TabIndex = 6;
            this.linkLabelSource.TabStop = true;
            this.linkLabelSource.Text = "Source Code";
            this.linkLabelSource.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.linkLabelSource.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelSource_LinkClicked);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 240);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(25, 0, 25, 0);
            this.label1.Size = new System.Drawing.Size(266, 27);
            this.label1.TabIndex = 7;
            this.label1.Text = "This software is distributed under the GNU General Public License v3.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // linkLabelLicense
            // 
            this.linkLabelLicense.AutoSize = true;
            this.linkLabelLicense.Location = new System.Drawing.Point(110, 267);
            this.linkLabelLicense.Name = "linkLabelLicense";
            this.linkLabelLicense.Size = new System.Drawing.Size(70, 13);
            this.linkLabelLicense.TabIndex = 8;
            this.linkLabelLicense.TabStop = true;
            this.linkLabelLicense.Text = "View License";
            this.linkLabelLicense.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelLicense_LinkClicked);
            // 
            // labelDonate
            // 
            this.labelDonate.Location = new System.Drawing.Point(12, 294);
            this.labelDonate.Name = "labelDonate";
            this.labelDonate.Padding = new System.Windows.Forms.Padding(25, 0, 25, 0);
            this.labelDonate.Size = new System.Drawing.Size(266, 40);
            this.labelDonate.TabIndex = 9;
            this.labelDonate.Text = "Bulk Automatic TV Rename Utility is free software. If you found it useful, please" +
    " send a donation to the author:";
            this.labelDonate.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // linkLabelDonate
            // 
            this.linkLabelDonate.AutoSize = true;
            this.linkLabelDonate.Location = new System.Drawing.Point(120, 334);
            this.linkLabelDonate.Name = "linkLabelDonate";
            this.linkLabelDonate.Size = new System.Drawing.Size(42, 13);
            this.linkLabelDonate.TabIndex = 10;
            this.linkLabelDonate.TabStop = true;
            this.linkLabelDonate.Text = "Donate";
            this.linkLabelDonate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelDonate_LinkClicked);
            // 
            // buttonClose
            // 
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonClose.Location = new System.Drawing.Point(102, 363);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(75, 23);
            this.buttonClose.TabIndex = 11;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // labelExtraCredits
            // 
            this.labelExtraCredits.Location = new System.Drawing.Point(12, 168);
            this.labelExtraCredits.Name = "labelExtraCredits";
            this.labelExtraCredits.Size = new System.Drawing.Size(266, 15);
            this.labelExtraCredits.TabIndex = 12;
            this.labelExtraCredits.Text = "Created with the following components:";
            this.labelExtraCredits.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // linkLabelObjectListView
            // 
            this.linkLabelObjectListView.Location = new System.Drawing.Point(12, 183);
            this.linkLabelObjectListView.Name = "linkLabelObjectListView";
            this.linkLabelObjectListView.Size = new System.Drawing.Size(266, 14);
            this.linkLabelObjectListView.TabIndex = 13;
            this.linkLabelObjectListView.TabStop = true;
            this.linkLabelObjectListView.Text = "ObjectListView";
            this.linkLabelObjectListView.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.linkLabelObjectListView.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelObjectListView_LinkClicked);
            // 
            // linkLabelIcon
            // 
            this.linkLabelIcon.Location = new System.Drawing.Point(12, 197);
            this.linkLabelIcon.Name = "linkLabelIcon";
            this.linkLabelIcon.Size = new System.Drawing.Size(266, 16);
            this.linkLabelIcon.TabIndex = 14;
            this.linkLabelIcon.TabStop = true;
            this.linkLabelIcon.Text = "Icon by Binassmax";
            this.linkLabelIcon.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.linkLabelIcon.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelIcon_LinkClicked);
            // 
            // linkLabelTVDB
            // 
            this.linkLabelTVDB.Location = new System.Drawing.Point(15, 212);
            this.linkLabelTVDB.Name = "linkLabelTVDB";
            this.linkLabelTVDB.Size = new System.Drawing.Size(263, 15);
            this.linkLabelTVDB.TabIndex = 15;
            this.linkLabelTVDB.TabStop = true;
            this.linkLabelTVDB.Text = "TheTVDB.com";
            this.linkLabelTVDB.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.linkLabelTVDB.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelTVDB_LinkClicked);
            // 
            // AboutDialog
            // 
            this.AcceptButton = this.buttonClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonClose;
            this.ClientSize = new System.Drawing.Size(290, 398);
            this.Controls.Add(this.linkLabelTVDB);
            this.Controls.Add(this.linkLabelIcon);
            this.Controls.Add(this.linkLabelObjectListView);
            this.Controls.Add(this.labelExtraCredits);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.linkLabelDonate);
            this.Controls.Add(this.labelDonate);
            this.Controls.Add(this.linkLabelLicense);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.linkLabelSource);
            this.Controls.Add(this.linkLabelHomepage);
            this.Controls.Add(this.linkLabelDavidProctor);
            this.Controls.Add(this.labelCredits);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.labelTitle);
            this.Controls.Add(this.pictureBoxIcon);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "AboutDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About BATRU";
            this.Load += new System.EventHandler(this.AboutDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIcon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxIcon;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelCredits;
        private System.Windows.Forms.LinkLabel linkLabelDavidProctor;
        private System.Windows.Forms.LinkLabel linkLabelHomepage;
        private System.Windows.Forms.LinkLabel linkLabelSource;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.LinkLabel linkLabelLicense;
        private System.Windows.Forms.Label labelDonate;
        private System.Windows.Forms.LinkLabel linkLabelDonate;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label labelExtraCredits;
        private System.Windows.Forms.LinkLabel linkLabelObjectListView;
        private System.Windows.Forms.LinkLabel linkLabelIcon;
        private System.Windows.Forms.LinkLabel linkLabelTVDB;
    }
}