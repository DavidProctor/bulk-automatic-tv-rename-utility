﻿namespace Interface
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStripTop = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectNewFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.offlineModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downloadMoreDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearCacheToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendFeedbackToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.donateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.labelSelectFolder = new System.Windows.Forms.Label();
            this.textBoxFolder = new System.Windows.Forms.TextBox();
            this.buttonBrowse = new System.Windows.Forms.Button();
            this.groupBoxEpisodeSelect = new System.Windows.Forms.GroupBox();
            this.treeListViewBrowser = new BrightIdeasSoftware.TreeListView();
            this.olvColumnFilename = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumnSeries = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumnSeason = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumnEpisode = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumnEpisodeTitle = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.imageListIcons = new System.Windows.Forms.ImageList(this.components);
            this.folderBrowserDialogTVFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.GroupBoxOutputOptions = new System.Windows.Forms.GroupBox();
            this.linkLabelTags = new System.Windows.Forms.LinkLabel();
            this.comboBoxSeasonEpisodeIdentifier = new System.Windows.Forms.ComboBox();
            this.labelSeasonEpisodeIdentifier = new System.Windows.Forms.Label();
            this.buttonMoveFilesBrowse = new System.Windows.Forms.Button();
            this.textBoxDestinationPath = new System.Windows.Forms.TextBox();
            this.labelOutputFolder = new System.Windows.Forms.Label();
            this.radioButtonLeaveFiles = new System.Windows.Forms.RadioButton();
            this.radioButtonMoveFiles = new System.Windows.Forms.RadioButton();
            this.radioButtonCopyFiles = new System.Windows.Forms.RadioButton();
            this.checkBoxRemoveYear = new System.Windows.Forms.CheckBox();
            this.labelSampleOutput = new System.Windows.Forms.Label();
            this.labelSampleOutputLabel = new System.Windows.Forms.Label();
            this.textBoxFilenameStructure = new System.Windows.Forms.TextBox();
            this.labelFilenameStructure = new System.Windows.Forms.Label();
            this.checkBoxFoldersForSeason = new System.Windows.Forms.CheckBox();
            this.checkBoxFoldersForSeries = new System.Windows.Forms.CheckBox();
            this.buttonGo = new System.Windows.Forms.Button();
            this.groupBoxSelectFolder = new System.Windows.Forms.GroupBox();
            this.errorProviderOutput = new System.Windows.Forms.ErrorProvider(this.components);
            this.menuStripTop.SuspendLayout();
            this.groupBoxEpisodeSelect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListViewBrowser)).BeginInit();
            this.GroupBoxOutputOptions.SuspendLayout();
            this.groupBoxSelectFolder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderOutput)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStripTop
            // 
            this.menuStripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStripTop.Location = new System.Drawing.Point(0, 0);
            this.menuStripTop.Name = "menuStripTop";
            this.menuStripTop.Size = new System.Drawing.Size(895, 24);
            this.menuStripTop.TabIndex = 0;
            this.menuStripTop.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectNewFolderToolStripMenuItem,
            this.toolStripSeparator2,
            this.offlineModeToolStripMenuItem,
            this.downloadMoreDetailsToolStripMenuItem,
            this.clearCacheToolStripMenuItem,
            this.toolStripSeparator3,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // selectNewFolderToolStripMenuItem
            // 
            this.selectNewFolderToolStripMenuItem.Name = "selectNewFolderToolStripMenuItem";
            this.selectNewFolderToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.selectNewFolderToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.selectNewFolderToolStripMenuItem.Text = "Select New Folder";
            this.selectNewFolderToolStripMenuItem.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(205, 6);
            // 
            // offlineModeToolStripMenuItem
            // 
            this.offlineModeToolStripMenuItem.Name = "offlineModeToolStripMenuItem";
            this.offlineModeToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.offlineModeToolStripMenuItem.Text = "Offline Mode";
            this.offlineModeToolStripMenuItem.Click += new System.EventHandler(this.offlineModeToolStripMenuItem_Click);
            // 
            // downloadMoreDetailsToolStripMenuItem
            // 
            this.downloadMoreDetailsToolStripMenuItem.Name = "downloadMoreDetailsToolStripMenuItem";
            this.downloadMoreDetailsToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.downloadMoreDetailsToolStripMenuItem.Text = "Download More Details";
            this.downloadMoreDetailsToolStripMenuItem.Click += new System.EventHandler(this.buttonDownload_Click);
            // 
            // clearCacheToolStripMenuItem
            // 
            this.clearCacheToolStripMenuItem.Name = "clearCacheToolStripMenuItem";
            this.clearCacheToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.clearCacheToolStripMenuItem.Text = "Clear Cache";
            this.clearCacheToolStripMenuItem.Click += new System.EventHandler(this.clearCacheToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(205, 6);
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sendFeedbackToolStripMenuItem,
            this.donateToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // sendFeedbackToolStripMenuItem
            // 
            this.sendFeedbackToolStripMenuItem.Name = "sendFeedbackToolStripMenuItem";
            this.sendFeedbackToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.sendFeedbackToolStripMenuItem.Text = "Send Feedback";
            this.sendFeedbackToolStripMenuItem.Click += new System.EventHandler(this.sendFeedbackToolStripMenuItem_Click);
            // 
            // donateToolStripMenuItem
            // 
            this.donateToolStripMenuItem.Name = "donateToolStripMenuItem";
            this.donateToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.donateToolStripMenuItem.Text = "Donate";
            this.donateToolStripMenuItem.Click += new System.EventHandler(this.donateToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(153, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // labelSelectFolder
            // 
            this.labelSelectFolder.AutoSize = true;
            this.labelSelectFolder.Location = new System.Drawing.Point(6, 16);
            this.labelSelectFolder.Name = "labelSelectFolder";
            this.labelSelectFolder.Size = new System.Drawing.Size(192, 13);
            this.labelSelectFolder.TabIndex = 0;
            this.labelSelectFolder.Text = "Select a &folder containing TV episodes:";
            // 
            // textBoxFolder
            // 
            this.textBoxFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFolder.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxFolder.Location = new System.Drawing.Point(7, 32);
            this.textBoxFolder.Name = "textBoxFolder";
            this.textBoxFolder.ReadOnly = true;
            this.textBoxFolder.Size = new System.Drawing.Size(773, 20);
            this.textBoxFolder.TabIndex = 1;
            this.textBoxFolder.Enter += new System.EventHandler(this.textBoxFolder_Enter);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBrowse.Location = new System.Drawing.Point(786, 30);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(75, 23);
            this.buttonBrowse.TabIndex = 2;
            this.buttonBrowse.Text = "&Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // groupBoxEpisodeSelect
            // 
            this.groupBoxEpisodeSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxEpisodeSelect.Controls.Add(this.treeListViewBrowser);
            this.groupBoxEpisodeSelect.Enabled = false;
            this.groupBoxEpisodeSelect.Location = new System.Drawing.Point(16, 90);
            this.groupBoxEpisodeSelect.Name = "groupBoxEpisodeSelect";
            this.groupBoxEpisodeSelect.Size = new System.Drawing.Size(867, 371);
            this.groupBoxEpisodeSelect.TabIndex = 2;
            this.groupBoxEpisodeSelect.TabStop = false;
            this.groupBoxEpisodeSelect.Text = "Select Episodes";
            // 
            // treeListViewBrowser
            // 
            this.treeListViewBrowser.AllColumns.Add(this.olvColumnFilename);
            this.treeListViewBrowser.AllColumns.Add(this.olvColumnSeries);
            this.treeListViewBrowser.AllColumns.Add(this.olvColumnSeason);
            this.treeListViewBrowser.AllColumns.Add(this.olvColumnEpisode);
            this.treeListViewBrowser.AllColumns.Add(this.olvColumnEpisodeTitle);
            this.treeListViewBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeListViewBrowser.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.SingleClick;
            this.treeListViewBrowser.CheckBoxes = true;
            this.treeListViewBrowser.CheckedAspectName = "";
            this.treeListViewBrowser.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumnFilename,
            this.olvColumnSeries,
            this.olvColumnSeason,
            this.olvColumnEpisode,
            this.olvColumnEpisodeTitle});
            this.treeListViewBrowser.HierarchicalCheckboxes = true;
            this.treeListViewBrowser.Location = new System.Drawing.Point(6, 19);
            this.treeListViewBrowser.Name = "treeListViewBrowser";
            this.treeListViewBrowser.OwnerDraw = true;
            this.treeListViewBrowser.ShowGroups = false;
            this.treeListViewBrowser.ShowImagesOnSubItems = true;
            this.treeListViewBrowser.Size = new System.Drawing.Size(854, 346);
            this.treeListViewBrowser.SmallImageList = this.imageListIcons;
            this.treeListViewBrowser.TabIndex = 0;
            this.treeListViewBrowser.UseCompatibleStateImageBehavior = false;
            this.treeListViewBrowser.View = System.Windows.Forms.View.Details;
            this.treeListViewBrowser.VirtualMode = true;
            this.treeListViewBrowser.CellEditFinishing += new BrightIdeasSoftware.CellEditEventHandler(this.treeListViewBrowser_CellEditFinishing);
            this.treeListViewBrowser.CellEditStarting += new BrightIdeasSoftware.CellEditEventHandler(this.treeListViewBrowser_CellEditStarting);
            this.treeListViewBrowser.FormatRow += new System.EventHandler<BrightIdeasSoftware.FormatRowEventArgs>(this.treeListViewBrowser_FormatRow);
            this.treeListViewBrowser.SelectionChanged += new System.EventHandler(this.treeListViewBrowser_SelectionChanged);
            // 
            // olvColumnFilename
            // 
            this.olvColumnFilename.AspectName = "Filename";
            this.olvColumnFilename.CellPadding = null;
            this.olvColumnFilename.IsEditable = false;
            this.olvColumnFilename.Text = "Current Filename";
            this.olvColumnFilename.Width = 269;
            // 
            // olvColumnSeries
            // 
            this.olvColumnSeries.AspectName = "SeriesTitle";
            this.olvColumnSeries.CellPadding = null;
            this.olvColumnSeries.Text = "Series";
            this.olvColumnSeries.Width = 194;
            // 
            // olvColumnSeason
            // 
            this.olvColumnSeason.AspectName = "Season";
            this.olvColumnSeason.CellPadding = null;
            this.olvColumnSeason.Text = "Season";
            this.olvColumnSeason.Width = 61;
            // 
            // olvColumnEpisode
            // 
            this.olvColumnEpisode.AspectName = "Episode";
            this.olvColumnEpisode.CellPadding = null;
            this.olvColumnEpisode.Text = "Episode";
            this.olvColumnEpisode.Width = 53;
            // 
            // olvColumnEpisodeTitle
            // 
            this.olvColumnEpisodeTitle.AspectName = "EpisodeTitle";
            this.olvColumnEpisodeTitle.CellPadding = null;
            this.olvColumnEpisodeTitle.Text = "Episode Title";
            this.olvColumnEpisodeTitle.Width = 152;
            // 
            // imageListIcons
            // 
            this.imageListIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListIcons.ImageStream")));
            this.imageListIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListIcons.Images.SetKeyName(0, "folder");
            this.imageListIcons.Images.SetKeyName(1, "screen");
            // 
            // folderBrowserDialogTVFolder
            // 
            this.folderBrowserDialogTVFolder.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowserDialogTVFolder.ShowNewFolderButton = false;
            // 
            // GroupBoxOutputOptions
            // 
            this.GroupBoxOutputOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GroupBoxOutputOptions.Controls.Add(this.linkLabelTags);
            this.GroupBoxOutputOptions.Controls.Add(this.comboBoxSeasonEpisodeIdentifier);
            this.GroupBoxOutputOptions.Controls.Add(this.labelSeasonEpisodeIdentifier);
            this.GroupBoxOutputOptions.Controls.Add(this.buttonMoveFilesBrowse);
            this.GroupBoxOutputOptions.Controls.Add(this.textBoxDestinationPath);
            this.GroupBoxOutputOptions.Controls.Add(this.labelOutputFolder);
            this.GroupBoxOutputOptions.Controls.Add(this.radioButtonLeaveFiles);
            this.GroupBoxOutputOptions.Controls.Add(this.radioButtonMoveFiles);
            this.GroupBoxOutputOptions.Controls.Add(this.radioButtonCopyFiles);
            this.GroupBoxOutputOptions.Controls.Add(this.checkBoxRemoveYear);
            this.GroupBoxOutputOptions.Controls.Add(this.labelSampleOutput);
            this.GroupBoxOutputOptions.Controls.Add(this.labelSampleOutputLabel);
            this.GroupBoxOutputOptions.Controls.Add(this.textBoxFilenameStructure);
            this.GroupBoxOutputOptions.Controls.Add(this.labelFilenameStructure);
            this.GroupBoxOutputOptions.Controls.Add(this.checkBoxFoldersForSeason);
            this.GroupBoxOutputOptions.Controls.Add(this.checkBoxFoldersForSeries);
            this.GroupBoxOutputOptions.Enabled = false;
            this.GroupBoxOutputOptions.Location = new System.Drawing.Point(16, 467);
            this.GroupBoxOutputOptions.Name = "GroupBoxOutputOptions";
            this.GroupBoxOutputOptions.Size = new System.Drawing.Size(867, 110);
            this.GroupBoxOutputOptions.TabIndex = 3;
            this.GroupBoxOutputOptions.TabStop = false;
            this.GroupBoxOutputOptions.Text = "Output Options";
            // 
            // linkLabelTags
            // 
            this.linkLabelTags.AutoSize = true;
            this.linkLabelTags.Location = new System.Drawing.Point(458, 44);
            this.linkLabelTags.Name = "linkLabelTags";
            this.linkLabelTags.Size = new System.Drawing.Size(19, 13);
            this.linkLabelTags.TabIndex = 10;
            this.linkLabelTags.TabStop = true;
            this.linkLabelTags.Text = "(?)";
            this.linkLabelTags.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelTags_LinkClicked);
            // 
            // comboBoxSeasonEpisodeIdentifier
            // 
            this.comboBoxSeasonEpisodeIdentifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSeasonEpisodeIdentifier.FormattingEnabled = true;
            this.comboBoxSeasonEpisodeIdentifier.Location = new System.Drawing.Point(486, 64);
            this.comboBoxSeasonEpisodeIdentifier.Name = "comboBoxSeasonEpisodeIdentifier";
            this.comboBoxSeasonEpisodeIdentifier.Size = new System.Drawing.Size(143, 21);
            this.comboBoxSeasonEpisodeIdentifier.TabIndex = 13;
            this.comboBoxSeasonEpisodeIdentifier.SelectedIndexChanged += new System.EventHandler(this.comboBoxSeasonEpisodeIdentifier_SelectedIndexChanged);
            // 
            // labelSeasonEpisodeIdentifier
            // 
            this.labelSeasonEpisodeIdentifier.AutoSize = true;
            this.labelSeasonEpisodeIdentifier.Location = new System.Drawing.Point(366, 67);
            this.labelSeasonEpisodeIdentifier.Name = "labelSeasonEpisodeIdentifier";
            this.labelSeasonEpisodeIdentifier.Size = new System.Drawing.Size(90, 13);
            this.labelSeasonEpisodeIdentifier.TabIndex = 12;
            this.labelSeasonEpisodeIdentifier.Text = "Episode identifier:";
            // 
            // buttonMoveFilesBrowse
            // 
            this.buttonMoveFilesBrowse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.errorProviderOutput.SetIconPadding(this.buttonMoveFilesBrowse, 5);
            this.buttonMoveFilesBrowse.Location = new System.Drawing.Point(767, 16);
            this.buttonMoveFilesBrowse.Name = "buttonMoveFilesBrowse";
            this.buttonMoveFilesBrowse.Size = new System.Drawing.Size(75, 23);
            this.buttonMoveFilesBrowse.TabIndex = 8;
            this.buttonMoveFilesBrowse.Text = "Browse";
            this.buttonMoveFilesBrowse.UseVisualStyleBackColor = true;
            this.buttonMoveFilesBrowse.Click += new System.EventHandler(this.buttonMoveFilesBrowse_Click);
            // 
            // textBoxDestinationPath
            // 
            this.textBoxDestinationPath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxDestinationPath.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxDestinationPath.ForeColor = System.Drawing.SystemColors.InfoText;
            this.textBoxDestinationPath.Location = new System.Drawing.Point(486, 18);
            this.textBoxDestinationPath.Name = "textBoxDestinationPath";
            this.textBoxDestinationPath.ReadOnly = true;
            this.textBoxDestinationPath.Size = new System.Drawing.Size(275, 20);
            this.textBoxDestinationPath.TabIndex = 7;
            // 
            // labelOutputFolder
            // 
            this.labelOutputFolder.AutoSize = true;
            this.labelOutputFolder.Location = new System.Drawing.Point(366, 21);
            this.labelOutputFolder.Name = "labelOutputFolder";
            this.labelOutputFolder.Size = new System.Drawing.Size(70, 13);
            this.labelOutputFolder.TabIndex = 6;
            this.labelOutputFolder.Text = "&Move files to:";
            // 
            // radioButtonLeaveFiles
            // 
            this.radioButtonLeaveFiles.AutoSize = true;
            this.radioButtonLeaveFiles.Location = new System.Drawing.Point(196, 65);
            this.radioButtonLeaveFiles.Name = "radioButtonLeaveFiles";
            this.radioButtonLeaveFiles.Size = new System.Drawing.Size(129, 17);
            this.radioButtonLeaveFiles.TabIndex = 5;
            this.radioButtonLeaveFiles.Text = "Leave all files in place";
            this.radioButtonLeaveFiles.UseVisualStyleBackColor = true;
            this.radioButtonLeaveFiles.CheckedChanged += new System.EventHandler(this.radioButtonLeaveFiles_CheckedChanged);
            // 
            // radioButtonMoveFiles
            // 
            this.radioButtonMoveFiles.AutoSize = true;
            this.radioButtonMoveFiles.Location = new System.Drawing.Point(196, 42);
            this.radioButtonMoveFiles.Name = "radioButtonMoveFiles";
            this.radioButtonMoveFiles.Size = new System.Drawing.Size(148, 17);
            this.radioButtonMoveFiles.TabIndex = 4;
            this.radioButtonMoveFiles.Text = "Move files to new location";
            this.radioButtonMoveFiles.UseVisualStyleBackColor = true;
            // 
            // radioButtonCopyFiles
            // 
            this.radioButtonCopyFiles.AutoSize = true;
            this.radioButtonCopyFiles.Checked = true;
            this.radioButtonCopyFiles.Location = new System.Drawing.Point(196, 20);
            this.radioButtonCopyFiles.Name = "radioButtonCopyFiles";
            this.radioButtonCopyFiles.Size = new System.Drawing.Size(145, 17);
            this.radioButtonCopyFiles.TabIndex = 3;
            this.radioButtonCopyFiles.TabStop = true;
            this.radioButtonCopyFiles.Text = "Copy files to new location";
            this.radioButtonCopyFiles.UseVisualStyleBackColor = true;
            // 
            // checkBoxRemoveYear
            // 
            this.checkBoxRemoveYear.AutoSize = true;
            this.checkBoxRemoveYear.Checked = true;
            this.checkBoxRemoveYear.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRemoveYear.Location = new System.Drawing.Point(7, 66);
            this.checkBoxRemoveYear.Name = "checkBoxRemoveYear";
            this.checkBoxRemoveYear.Size = new System.Drawing.Size(176, 17);
            this.checkBoxRemoveYear.TabIndex = 2;
            this.checkBoxRemoveYear.Text = "Remove year from series names";
            this.checkBoxRemoveYear.UseVisualStyleBackColor = true;
            this.checkBoxRemoveYear.CheckedChanged += new System.EventHandler(this.checkBoxRemoveYear_CheckedChanged);
            // 
            // labelSampleOutput
            // 
            this.labelSampleOutput.AutoSize = true;
            this.labelSampleOutput.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSampleOutput.Location = new System.Drawing.Point(483, 90);
            this.labelSampleOutput.Name = "labelSampleOutput";
            this.labelSampleOutput.Size = new System.Drawing.Size(125, 13);
            this.labelSampleOutput.TabIndex = 15;
            this.labelSampleOutput.Text = "<SAMPLE OUTPUT>";
            // 
            // labelSampleOutputLabel
            // 
            this.labelSampleOutputLabel.AutoSize = true;
            this.labelSampleOutputLabel.Location = new System.Drawing.Point(366, 90);
            this.labelSampleOutputLabel.Name = "labelSampleOutputLabel";
            this.labelSampleOutputLabel.Size = new System.Drawing.Size(78, 13);
            this.labelSampleOutputLabel.TabIndex = 14;
            this.labelSampleOutputLabel.Text = "Sample output:";
            // 
            // textBoxFilenameStructure
            // 
            this.textBoxFilenameStructure.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.errorProviderOutput.SetIconPadding(this.textBoxFilenameStructure, 5);
            this.textBoxFilenameStructure.Location = new System.Drawing.Point(486, 41);
            this.textBoxFilenameStructure.Name = "textBoxFilenameStructure";
            this.textBoxFilenameStructure.Size = new System.Drawing.Size(356, 20);
            this.textBoxFilenameStructure.TabIndex = 11;
            this.textBoxFilenameStructure.Text = "[SERIES] - [IDENTIFIER] - [EPISODE_TITLE]";
            this.textBoxFilenameStructure.TextChanged += new System.EventHandler(this.textBoxFilenameStructure_TextChanged);
            this.textBoxFilenameStructure.Validated += new System.EventHandler(this.textBoxFilenameStructure_Validated);
            // 
            // labelFilenameStructure
            // 
            this.labelFilenameStructure.AutoSize = true;
            this.labelFilenameStructure.Location = new System.Drawing.Point(366, 44);
            this.labelFilenameStructure.Name = "labelFilenameStructure";
            this.labelFilenameStructure.Size = new System.Drawing.Size(96, 13);
            this.labelFilenameStructure.TabIndex = 9;
            this.labelFilenameStructure.Text = "File&name structure:";
            // 
            // checkBoxFoldersForSeason
            // 
            this.checkBoxFoldersForSeason.AutoSize = true;
            this.checkBoxFoldersForSeason.Checked = true;
            this.checkBoxFoldersForSeason.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFoldersForSeason.Location = new System.Drawing.Point(7, 43);
            this.checkBoxFoldersForSeason.Name = "checkBoxFoldersForSeason";
            this.checkBoxFoldersForSeason.Size = new System.Drawing.Size(174, 17);
            this.checkBoxFoldersForSeason.TabIndex = 1;
            this.checkBoxFoldersForSeason.Text = "Create a folder for each season";
            this.checkBoxFoldersForSeason.UseVisualStyleBackColor = true;
            // 
            // checkBoxFoldersForSeries
            // 
            this.checkBoxFoldersForSeries.AutoSize = true;
            this.checkBoxFoldersForSeries.Checked = true;
            this.checkBoxFoldersForSeries.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFoldersForSeries.Location = new System.Drawing.Point(7, 20);
            this.checkBoxFoldersForSeries.Name = "checkBoxFoldersForSeries";
            this.checkBoxFoldersForSeries.Size = new System.Drawing.Size(167, 17);
            this.checkBoxFoldersForSeries.TabIndex = 0;
            this.checkBoxFoldersForSeries.Text = "Create a folder for each series";
            this.checkBoxFoldersForSeries.UseVisualStyleBackColor = true;
            this.checkBoxFoldersForSeries.CheckedChanged += new System.EventHandler(this.checkBoxFoldersForSeries_CheckedChanged);
            // 
            // buttonGo
            // 
            this.buttonGo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGo.Enabled = false;
            this.buttonGo.Location = new System.Drawing.Point(739, 583);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(144, 45);
            this.buttonGo.TabIndex = 4;
            this.buttonGo.Text = "Organize Checked Files";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // groupBoxSelectFolder
            // 
            this.groupBoxSelectFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSelectFolder.Controls.Add(this.buttonBrowse);
            this.groupBoxSelectFolder.Controls.Add(this.labelSelectFolder);
            this.groupBoxSelectFolder.Controls.Add(this.textBoxFolder);
            this.groupBoxSelectFolder.Location = new System.Drawing.Point(16, 27);
            this.groupBoxSelectFolder.Name = "groupBoxSelectFolder";
            this.groupBoxSelectFolder.Size = new System.Drawing.Size(867, 57);
            this.groupBoxSelectFolder.TabIndex = 1;
            this.groupBoxSelectFolder.TabStop = false;
            this.groupBoxSelectFolder.Text = "Select Source Folder";
            // 
            // errorProviderOutput
            // 
            this.errorProviderOutput.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
            this.errorProviderOutput.ContainerControl = this;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 640);
            this.Controls.Add(this.groupBoxSelectFolder);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.GroupBoxOutputOptions);
            this.Controls.Add(this.groupBoxEpisodeSelect);
            this.Controls.Add(this.menuStripTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripTop;
            this.MinimumSize = new System.Drawing.Size(787, 405);
            this.Name = "MainForm";
            this.Text = "Bulk Automatic TV Rename Utility";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuStripTop.ResumeLayout(false);
            this.menuStripTop.PerformLayout();
            this.groupBoxEpisodeSelect.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListViewBrowser)).EndInit();
            this.GroupBoxOutputOptions.ResumeLayout(false);
            this.GroupBoxOutputOptions.PerformLayout();
            this.groupBoxSelectFolder.ResumeLayout(false);
            this.groupBoxSelectFolder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderOutput)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripTop;
        private System.Windows.Forms.Label labelSelectFolder;
        private System.Windows.Forms.TextBox textBoxFolder;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.GroupBox groupBoxEpisodeSelect;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogTVFolder;
        private BrightIdeasSoftware.TreeListView treeListViewBrowser;
        private BrightIdeasSoftware.OLVColumn olvColumnFilename;
        private BrightIdeasSoftware.OLVColumn olvColumnSeries;
        private BrightIdeasSoftware.OLVColumn olvColumnSeason;
        private BrightIdeasSoftware.OLVColumn olvColumnEpisode;
        private BrightIdeasSoftware.OLVColumn olvColumnEpisodeTitle;
        private System.Windows.Forms.GroupBox GroupBoxOutputOptions;
        private System.Windows.Forms.CheckBox checkBoxFoldersForSeason;
        private System.Windows.Forms.CheckBox checkBoxFoldersForSeries;
        private System.Windows.Forms.CheckBox checkBoxRemoveYear;
        private System.Windows.Forms.Label labelSampleOutput;
        private System.Windows.Forms.Label labelSampleOutputLabel;
        private System.Windows.Forms.TextBox textBoxFilenameStructure;
        private System.Windows.Forms.Label labelFilenameStructure;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.RadioButton radioButtonLeaveFiles;
        private System.Windows.Forms.RadioButton radioButtonMoveFiles;
        private System.Windows.Forms.RadioButton radioButtonCopyFiles;
        private System.Windows.Forms.Button buttonMoveFilesBrowse;
        private System.Windows.Forms.TextBox textBoxDestinationPath;
        private System.Windows.Forms.Label labelOutputFolder;
        private System.Windows.Forms.GroupBox groupBoxSelectFolder;
        private System.Windows.Forms.ToolStripMenuItem offlineModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectNewFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearCacheToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downloadMoreDetailsToolStripMenuItem;
        private System.Windows.Forms.ErrorProvider errorProviderOutput;
        private System.Windows.Forms.ImageList imageListIcons;
        private System.Windows.Forms.ComboBox comboBoxSeasonEpisodeIdentifier;
        private System.Windows.Forms.Label labelSeasonEpisodeIdentifier;
        private System.Windows.Forms.ToolStripMenuItem donateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.LinkLabel linkLabelTags;
        private System.Windows.Forms.ToolStripMenuItem sendFeedbackToolStripMenuItem;
    }
}

