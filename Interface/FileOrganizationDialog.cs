﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface
{
    /// <summary>
    /// A dialog that organizes episodes selected in MainForm and shows a progress bar.
    /// </summary>
    public partial class FileOrganizationDialog : Form
    {
        private BackgroundWorker _worker;
        private string _repeatAction;

        /// <summary>
        /// Standard Form constructor.
        /// </summary>
        public FileOrganizationDialog()
        {
            InitializeComponent();
            _repeatAction = string.Empty;
        }

        /// <summary>
        /// Upon loading the dialog, sets up the BackgroundWorker and begins the asynchronous process.
        /// </summary>
        /// <param name="sender">Standard form parameter.</param>
        /// <param name="e">Standard form parameter.</param>
        private void FileOrganizationDialog_Load(object sender, EventArgs e)
        {
            _worker = new BackgroundWorker();
            _worker.DoWork += worker_DoWork;
            _worker.ProgressChanged += worker_ProgressChanged;
            _worker.WorkerReportsProgress = true;
            _worker.RunWorkerCompleted += worker_RunWorkerCompleted;
            _worker.RunWorkerAsync();
        }

        /// <summary>
        /// Iterates through the episodes to organize and reports progress after each.
        /// </summary>
        /// <param name="sender">Standard form parameter.</param>
        /// <param name="e">Standard form parameter.</param>
        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            List<ShowProxy> directories = new List<ShowProxy>();
            for (int i = 0; i < Proxies.Count; i++)
            {
                try
                {
                    if (Proxies[i].HasChildren)
                    {
                        directories.Add(Proxies[i]);
                    }
                    else
                    {
                        Proxies[i].Organize();
                    }
                }
                // If there is already a file with this name
                catch (Batru.FileConflictException ex)
                {
                    string action = string.Empty;

                    // Ask the user what to do
                    if (_repeatAction == string.Empty)
                    {
                        FileConflictDialog dlg = new FileConflictDialog();
                        dlg.Filename = ex.Filename;
                        dlg.Directory = ex.Directory;
                        dlg.ShowDialog();

                        // Commit to overwriting the file
                        if (dlg.DialogResult == DialogResult.Yes)
                        {
                            action = "overwrite";
                            if (dlg.RepeatAction)
                            {
                                _repeatAction = "overwrite";
                            }
                        }
                        // Commit to skipping the file
                        else if (dlg.DialogResult == DialogResult.No)
                        {
                            action = "skip";
                            if (dlg.RepeatAction)
                            {
                                _repeatAction = "skip";
                            }
                        }
                    }

                    string fullPath = ex.Directory + "\\" + ex.Filename;

                    // Overwrite the file
                    if (action == "overwrite" || _repeatAction == "overwrite")
                    {
                        BatruFacade.DeleteFile(fullPath);
                        i--;
                        continue;
                    }
                    // The file will be automatically skipped otherwise
                }
                int progress = (i + 1) * 100 / Proxies.Count;
                _worker.ReportProgress(progress);
            }

            foreach (ShowProxy dir in directories)
            {
                dir.Organize();
            }
        }

        /// <summary>
        /// Changes the value of progressBarFileOrganization to reflect progress in organizing files.
        /// </summary>
        /// <param name="sender">Standard form parameter.</param>
        /// <param name="e">Standard form parameter, including current progress percentage.</param>
        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBarFileOrganization.Value = e.ProgressPercentage;
        }

        /// <summary>
        /// Closes the dialog successfully.
        /// </summary>
        /// <param name="sender">Standard form parameter.</param>
        /// <param name="e">Standard form parameter.</param>
        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        /// <summary>
        /// The ShowProxy objects selected to be organized.
        /// </summary>
        public List<ShowProxy> Proxies
        {
            get;
            set;
        }
    }
}
