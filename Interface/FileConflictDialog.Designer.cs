﻿namespace Interface
{
    partial class FileConflictDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelConflict = new System.Windows.Forms.Label();
            this.labelPrompt = new System.Windows.Forms.Label();
            this.buttonOverwrite = new System.Windows.Forms.Button();
            this.buttonSkip = new System.Windows.Forms.Button();
            this.checkBoxAllFiles = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // labelConflict
            // 
            this.labelConflict.Location = new System.Drawing.Point(12, 9);
            this.labelConflict.Name = "labelConflict";
            this.labelConflict.Size = new System.Drawing.Size(334, 44);
            this.labelConflict.TabIndex = 0;
            this.labelConflict.Text = "There is already a file named {0} in {1}.";
            // 
            // labelPrompt
            // 
            this.labelPrompt.AutoSize = true;
            this.labelPrompt.Location = new System.Drawing.Point(15, 57);
            this.labelPrompt.Name = "labelPrompt";
            this.labelPrompt.Size = new System.Drawing.Size(159, 13);
            this.labelPrompt.TabIndex = 1;
            this.labelPrompt.Text = "How would you like to proceed?";
            // 
            // buttonOverwrite
            // 
            this.buttonOverwrite.Location = new System.Drawing.Point(190, 104);
            this.buttonOverwrite.Name = "buttonOverwrite";
            this.buttonOverwrite.Size = new System.Drawing.Size(75, 23);
            this.buttonOverwrite.TabIndex = 3;
            this.buttonOverwrite.Text = "&Overwrite";
            this.buttonOverwrite.UseVisualStyleBackColor = true;
            this.buttonOverwrite.Click += new System.EventHandler(this.buttonOverwrite_Click);
            // 
            // buttonSkip
            // 
            this.buttonSkip.Location = new System.Drawing.Point(271, 104);
            this.buttonSkip.Name = "buttonSkip";
            this.buttonSkip.Size = new System.Drawing.Size(75, 23);
            this.buttonSkip.TabIndex = 5;
            this.buttonSkip.Text = "&Skip";
            this.buttonSkip.UseVisualStyleBackColor = true;
            this.buttonSkip.Click += new System.EventHandler(this.buttonSkip_Click);
            // 
            // checkBoxAllFiles
            // 
            this.checkBoxAllFiles.AutoSize = true;
            this.checkBoxAllFiles.Location = new System.Drawing.Point(217, 81);
            this.checkBoxAllFiles.Name = "checkBoxAllFiles";
            this.checkBoxAllFiles.Size = new System.Drawing.Size(129, 17);
            this.checkBoxAllFiles.TabIndex = 2;
            this.checkBoxAllFiles.Text = "&Do this for all conflicts";
            this.checkBoxAllFiles.UseVisualStyleBackColor = true;
            this.checkBoxAllFiles.CheckedChanged += new System.EventHandler(this.checkBoxAllFiles_CheckedChanged);
            // 
            // FileConflictDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 139);
            this.Controls.Add(this.checkBoxAllFiles);
            this.Controls.Add(this.buttonSkip);
            this.Controls.Add(this.buttonOverwrite);
            this.Controls.Add(this.labelPrompt);
            this.Controls.Add(this.labelConflict);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FileConflictDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "File Conflict";
            this.Load += new System.EventHandler(this.FileConflictDialog_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelConflict;
        private System.Windows.Forms.Label labelPrompt;
        private System.Windows.Forms.Button buttonOverwrite;
        private System.Windows.Forms.Button buttonSkip;
        private System.Windows.Forms.CheckBox checkBoxAllFiles;

    }
}