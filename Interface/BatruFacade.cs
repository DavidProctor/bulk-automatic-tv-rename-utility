﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Batru;

namespace Interface
{
    /// <summary>
    /// Provides Interface with access to Batru's static classes 
    /// </summary>
    class BatruFacade
    {
        public static bool offlineMode;

        private static FileOrganizationSettings _settings;

        /// <summary>
        /// Sets offline mode to the given setting.
        /// </summary>
        /// <param name="setting">The value to set offline mode to.</param>
        public static void SetOfflineMode(bool setting)
        {
            offlineMode = setting;
            SeriesFactory.OfflineMode = setting;
        }

        /// <summary>
        /// Creates an instance of OrganizationSettings based on the supplied value.
        /// </summary>
        /// <param name="folderForSeries">Whether to create a folder for each series.</param>
        /// <param name="folderForSeason">Whether to create a folder for each season.</param>
        /// <param name="folderForSpecials">Whether to create a folder for specials.</param>
        /// <param name="removeYearFromTitles">Whether to remove year suffixes from series titles.</param>
        /// <param name="relocationOption">Whether to copy the files, move them, or rename them in place.</param>
        /// <param name="destinationPath">The folder to move the files to.</param>
        /// <param name="filenameStructure">The pattern to chagne all filenames to.</param>
        /// <param name="identifierStyle">The pattern to enforce for season/episode identifiers.</param>
        public static void CreateOrganizationSettings(bool folderForSeries, bool folderForSeason, bool removeYearFromTitles,
            int relocationOption, string destinationPath, string filenameStructure, string identifierStyle)
        {
            _settings = FileOrganizationSettings.GetInstance();

            _settings.FolderForSeries = folderForSeries;
            _settings.FolderForSeason = folderForSeason;
            //_settings.FolderForSpecials = folderForSpecials;
            _settings.RemoveYearFromTitles = removeYearFromTitles;
            _settings.FilenameStructure = filenameStructure;
            _settings.IdentifierStyle = identifierStyle;

            if (relocationOption == 1)
            {
                _settings.RelocationOption = RelocationOptions.Copy;
            }
            else if (relocationOption == 2)
            {
                _settings.RelocationOption = RelocationOptions.Move;
            }
            else if (relocationOption == 3)
            {
                _settings.RelocationOption = RelocationOptions.InPlace;
            }
            else
            {
                _settings = null;
                throw new ArgumentOutOfRangeException("Invalid relocationOption value: " + relocationOption);
            }

            if (_settings.RelocationOption != RelocationOptions.InPlace)
            {
                if (System.IO.Directory.Exists(destinationPath))
                {
                    _settings.SetDestinationFolder(destinationPath);
                }
                else
                {
                    _settings = null;
                    throw new ArgumentException("Relocation destination folder is invalid.");
                }
            }
        }

        /// <summary>
        /// Clears Batru.SeriesFactory._allSeries.
        /// </summary>
        public static void ClearCache()
        {
            SeriesFactory.ClearCache();
        }

        /// <summary>
        /// Returns the most recently set FileOrganizationSettings.
        /// </summary>
        /// <returns>The most recently set FileOrganizationSettings.</returns>
        public static FileOrganizationSettings GetOrganizationSettings()
        {
            return _settings;
        }

        /// <summary>
        /// Deletes the file at the given path.
        /// </summary>
        /// <param name="fullPath">The full path to the file to be deleted.</param>
        public static void DeleteFile(string fullPath)
        {
            FileOrganizer.DeleteFile(fullPath);
        }
    }
}
