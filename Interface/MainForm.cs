﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface
{
    /// <summary>
    /// The main interface for the program.
    /// </summary>
    public partial class MainForm : Form
    {
        private ShowProxy[] _rootArray;
        private ShowProxy _root;
        private bool _suppressMultipleEditWarning;
        private ShowProxy _sampleSource;

        /// <summary>
        /// Constructor. Sets up defaults.
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
            _suppressMultipleEditWarning = false;
            treeListViewBrowser.GetColumn(0).ImageGetter = new BrightIdeasSoftware.ImageGetterDelegate(RowImageGetter);
        }

        /// <summary>
        /// Upon loading, populate labelSampleOutput and the contents of comboBoxSeasonEpisodeIdentifier
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainForm_Load(object sender, EventArgs e)
        {
            populateSampleOutput();
            Dictionary<string, string> identifierStyles = new Dictionary<string, string>
            {
                {"1x01", "[SEASON]x[EPISODE]"},
                {"s1e01", "s[SEASON]e[EPISODE]"},
                {"Season 1 Episode 1", "Season [SEASON] Episode [EPISODE]"}
            };
            comboBoxSeasonEpisodeIdentifier.DataSource = new BindingSource(identifierStyles, null);
            comboBoxSeasonEpisodeIdentifier.DisplayMember = "Key";
            comboBoxSeasonEpisodeIdentifier.ValueMember = "Value";
        }

        /// <summary>
        /// Prevents the cursor from showing up in the read-only textBoxFolder.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void textBoxFolder_Enter(object sender, EventArgs e)
        {
            buttonBrowse.Select();
        }

        /// <summary>
        /// Selects a root folder to search for episodes.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            // Check for a valid path
            if (folderBrowserDialogTVFolder.ShowDialog() == DialogResult.OK)
            {
                textBoxFolder.Text = folderBrowserDialogTVFolder.SelectedPath;
                groupBoxEpisodeSelect.Enabled = true;
                GroupBoxOutputOptions.Enabled = true;
                buttonGo.Enabled = true;
                PopulateTreeListViewBrowser(folderBrowserDialogTVFolder.SelectedPath);
            }
        }

        /// <summary>
        /// Highlights non-selected treeListViewBrowser nodes in red.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void treeListViewBrowser_FormatRow(object sender, BrightIdeasSoftware.FormatRowEventArgs e)
        {
            ShowProxy proxy = e.Model as ShowProxy;
            if (!e.Item.Checked)
            {
                e.Item.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// Configures controls when the user edits a cell.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void treeListViewBrowser_CellEditStarting(object sender, BrightIdeasSoftware.CellEditEventArgs e)
        {
            ShowProxy item = e.RowObject as ShowProxy;
            populateSampleOutput(item);

            // Use custom control for scrolling through episode and season numbers
            NumericUpDownOptionalText updown = new NumericUpDownOptionalText();

            // Substitute words for some episode values
            if (e.Column.AspectName == "Season")
            {
                updown.Replacements = ShowProxy.seasonReplacements;
                updown.Minimum = -2;
                updown.Value = item.SeasonNum;
                updown.ForceReplacements();
            }
            else if (e.Column.AspectName == "Episode")
            {
                updown.Replacements = ShowProxy.episodeReplacements;
                updown.Minimum = -1;
                updown.Value = item.EpisodeNum;
                updown.ForceReplacements();
            }
            else if (e.Column.AspectName == "SeriesTitle")
            {
                ComboBox seriesSelector = new ComboBox();
                seriesSelector.DataSource = item.seriesCandidateList;
                seriesSelector.Bounds = e.CellBounds;
                seriesSelector.Tag = item;
                e.Control = seriesSelector;
                return;
            }
            // If this column is not one of the ones listed above, do not use a custom control
            else
            {
                e.Control.Tag = e.RowObject;
                return;
            }

            // Set up the custom control for season and episode numbers
            updown.Bounds = e.CellBounds;
            updown.Tag = item;
            e.Control = updown;
        }

        /// <summary>
        /// Analyzes new values and commits edits when the user finishes editing a cell.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void treeListViewBrowser_CellEditFinishing(object sender, BrightIdeasSoftware.CellEditEventArgs e)
        {
            ShowProxy item = e.Control.Tag as ShowProxy;

            if (e.Column.AspectName == "Season")
            {
                if (item.HasChildren && !_suppressMultipleEditWarning)
                {
                    MultipleEditWarningDialog dlg = new MultipleEditWarningDialog();
                    dlg.ShowDialog();
                    if (dlg.DialogResult == DialogResult.Yes)
                    {
                        item.SeasonNum = Convert.ToInt32(e.NewValue);
                    }
                    _suppressMultipleEditWarning = dlg.DontShowAgain;
                }
                else
                {
                    item.SeasonNum = Convert.ToInt32(e.NewValue);
                }
            }
            else if (e.Column.AspectName == "Episode")
            {
                if (item.HasChildren && !_suppressMultipleEditWarning)
                {
                    MultipleEditWarningDialog dlg = new MultipleEditWarningDialog();
                    dlg.ShowDialog();
                    if (dlg.DialogResult == DialogResult.Yes)
                    {
                        item.EpisodeNum = Convert.ToInt32(e.NewValue);
                    }
                    _suppressMultipleEditWarning = dlg.DontShowAgain;
                }
                else
                {
                    item.EpisodeNum = Convert.ToInt32(e.NewValue);
                }
            }
            else if (e.Column.AspectName == "SeriesTitle")
            {
                if (item.SeriesTitle == e.Control.Text)
                {
                    // User did not change series title - do nothing
                }
                else if (item.HasChildren && !_suppressMultipleEditWarning)
                {
                    MultipleEditWarningDialog dlg = new MultipleEditWarningDialog();
                    dlg.ShowDialog();
                    if (dlg.DialogResult == DialogResult.Yes)
                    {
                        item.SeriesTitle = e.Control.Text;
                        IdentifyWithChildren(item);
                    }
                    else
                    {
                        e.Cancel = true;
                        Cursor = Cursors.Default;
                    }
                    _suppressMultipleEditWarning = dlg.DontShowAgain;
                }
                else
                {
                    item.SeriesTitle = e.Control.Text;
                    IdentifyWithChildren(item);
                }
            }

            populateSampleOutput();
        }

        /// <summary>
        /// Downloads extra details on all ShowNodes. Called from File > Download Extra Details
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void buttonDownload_Click(object sender, EventArgs e)
        {
            if (_root == null)
            {
                return;
            }
            PopulateTreeListViewBrowser(textBoxFolder.Text);
        }

        /// <summary>
        /// Toggles Offline Mode.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void offlineModeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            offlineModeToolStripMenuItem.Checked = !offlineModeToolStripMenuItem.Checked;
            BatruFacade.SetOfflineMode(offlineModeToolStripMenuItem.Checked);
            if (offlineModeToolStripMenuItem.Checked)
            {
                this.Text = "Bulk Automatic TV Rename Utility [Offline Mode]";
            }
            else
            {
                this.Text = "Bulk Automatic TV Rename Utility";
            }
        }

        /// <summary>
        /// Fully clears the cache and resets this Form to its starting state.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void clearCacheToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _rootArray = null;
            treeListViewBrowser.ClearObjects();
            treeListViewBrowser.ClearCachedInfo();
            BatruFacade.ClearCache();
            textBoxFolder.Text = string.Empty;
            groupBoxEpisodeSelect.Enabled = false;
            GroupBoxOutputOptions.Enabled = false;
            buttonGo.Enabled = false;
        }

        /// <summary>
        /// Validates output settings, creates FileOrganizationSettings, passes the selected files to FileOrganizationDialog for processing, and refreshes treeListViewBrowser.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void buttonGo_Click(object sender, EventArgs e)
        {
            // Validate filename structure
            if (!FilenameStructureIsValid())
            {
                string message = "Your filename structure must include at least a season and episode identifier. Use the [IDENTIFIER] tags.";
                MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                errorProviderOutput.SetError(textBoxFilenameStructure, message);
                return;
            }

            // Set relocation option
            int relocationOption = 0;
            if (radioButtonCopyFiles.Checked)
            {
                relocationOption = 1;
            }
            else if (radioButtonMoveFiles.Checked)
            {
                relocationOption = 2;
            }
            else if (radioButtonLeaveFiles.Checked)
            {
                relocationOption = 3;
            }

            // Attempt to create FileOrganizationSettings
            try
            {
                BatruFacade.CreateOrganizationSettings(
                    checkBoxFoldersForSeries.Checked,
                    checkBoxFoldersForSeason.Checked,
                    //checkBoxFolderForSpecials.Checked,
                    checkBoxRemoveYear.Checked,
                    relocationOption,
                    textBoxDestinationPath.Text,
                    textBoxFilenameStructure.Text,
                    comboBoxSeasonEpisodeIdentifier.SelectedValue.ToString()
                    );
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Invalid destination folder. Please choose a valid folder to move or copy your files to.",
                    "Invalid Folder",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
                errorProviderOutput.SetError(buttonMoveFilesBrowse, "Choose a valid folder to move or copy your files to.");
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format("An error occurred. Please try again.\n\nFull details:\n\n{0}: {1}", ex.GetType(), ex.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
                return;
            }

            // Create FileOrganizationDialog and pass it the ShowProxies to organize
            FileOrganizationDialog dlg = new FileOrganizationDialog();
            dlg.Proxies = treeListViewBrowser.CheckedObjectsEnumerable.Cast<ShowProxy>().ToList();
            dlg.ShowDialog();

            // Refresh treeListViewBrowser
            treeListViewBrowser.ClearCachedInfo();
            treeListViewBrowser.ClearObjects();
            PopulateTreeListViewBrowser(textBoxFolder.Text);
        }

        /// <summary>
        /// Allows the user to set the output folder.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void buttonMoveFilesBrowse_Click(object sender, EventArgs e)
        {
            errorProviderOutput.SetError(buttonMoveFilesBrowse, string.Empty);

            folderBrowserDialogTVFolder.ShowNewFolderButton = true;
            if (folderBrowserDialogTVFolder.ShowDialog() == DialogResult.OK)
            {
                textBoxDestinationPath.Text = folderBrowserDialogTVFolder.SelectedPath;
            }
            folderBrowserDialogTVFolder.ShowNewFolderButton = false;
        }

        /// <summary>
        /// Refresh sample output as the user types into textBoxFilenameStructure.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void textBoxFilenameStructure_TextChanged(object sender, EventArgs e)
        {
            populateSampleOutput();
        }

        /// <summary>
        /// Refresh sample output as the user checks and unchecks checkBoxRemoveYear.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void checkBoxRemoveYear_CheckedChanged(object sender, EventArgs e)
        {
            populateSampleOutput();
        }

        /// <summary>
        /// Refresh sample output as the user selects different identifier styles.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void comboBoxSeasonEpisodeIdentifier_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateSampleOutput();
        }

        /// <summary>
        /// Enable/disable the controls that become redundant as radioButtonLeaveFiles is checked and unchecked.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void radioButtonLeaveFiles_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonLeaveFiles.Checked == true)
            {
                textBoxDestinationPath.Enabled = false;
                buttonMoveFilesBrowse.Enabled = false;
                checkBoxFoldersForSeries.Enabled = false;
                checkBoxFoldersForSeason.Enabled = false;
                //checkBoxFolderForSpecials.Enabled = false;
                errorProviderOutput.SetError(buttonMoveFilesBrowse, string.Empty);
            }
            else
            {
                textBoxDestinationPath.Enabled = true;
                buttonMoveFilesBrowse.Enabled = true;
                checkBoxFoldersForSeries.Enabled = true;
                checkBoxFoldersForSeason.Enabled = true;
                //checkBoxFolderForSpecials.Enabled = true;
            }
        }

        /// <summary>
        /// Remove the errorProvider from the filename structure controls if a valid filename structure has been set.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void textBoxFilenameStructure_Validated(object sender, EventArgs e)
        {
            if (FilenameStructureIsValid())
            {
                errorProviderOutput.SetError(textBoxFilenameStructure, string.Empty);
            }
        }

        /// <summary>
        /// Enable and disable controls that become unimportant based on the state of checkBoxFoldersForSeries.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void checkBoxFoldersForSeries_CheckedChanged(object sender, EventArgs e)
        {
            bool check = checkBoxFoldersForSeries.Checked;
            checkBoxFoldersForSeason.Checked = check;
            checkBoxFoldersForSeason.Enabled = check;
            //checkBoxFolderForSpecials.Checked = check;
            //checkBoxFolderForSpecials.Enabled = check;
        }

        /// <summary>
        /// Populates the sample output based on the selected row in treeListViewBrowser.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void treeListViewBrowser_SelectionChanged(object sender, EventArgs e)
        {
            if (treeListViewBrowser.SelectedItem == null)
            {
                return;
            }
            ShowProxy source = treeListViewBrowser.SelectedItem.RowObject as ShowProxy;
            populateSampleOutput(source);
        }

        /// <summary>
        /// Opens the "about" menu.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutDialog dlg = new AboutDialog();
            dlg.Show();
        }

        /// <summary>
        /// Links the user to the donation button.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void donateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://davidproctor.ca/?p=86#donate");
        }

        /// <summary>
        /// Shows instructions for writing filename formats.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void linkLabelTags_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string output = "Write a template indicating how you want to format the filenames of the renamed files. ";
            output += "Everything you type will be included, except that the following tags will be replaced as follows:";
            output += "\n\n\t[SERIES] - the name of the series";
            output += "\n\t[IDENTIFIER] - the season/episode number in the style you indicate";
            output += "\n\t[EPISODE_NAME] - the name of the episode, if known";
            output += "\n\nClick on a filename to see a preview of its re-formatted name below.";
            MessageBox.Show(output, "Tagging help", MessageBoxButtons.OK);
        }

        /// <summary>
        /// Links the user to the comments section on the homepage.
        /// </summary>
        /// <param name="sender">Default event parameter.</param>
        /// <param name="e">Default event parameter.</param>
        private void sendFeedbackToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.davidproctor.ca/?p=86#respond");
        }

        /// <summary>
        /// Places a folder or screen icon next to the filename in each row in the treeListViewBrowser.
        /// </summary>
        /// <param name="rowObject">Default event parameter.</param>
        /// <returns>Default event parameter.</returns>
        private object RowImageGetter(object rowObject)
        {
            ShowProxy proxy = rowObject as ShowProxy;

            if (proxy.HasChildren)
            {
                return "folder";
            }
            else
            {
                return "screen";
            }
        }

        /// <summary>
        /// Populates labelSampleOutput based on the current filename structure and either the given ShowProxy or the default.
        /// </summary>
        /// <param name="source">The ShowProxy whose details are to be used to populate sample output.</param>
        private void populateSampleOutput(ShowProxy source = null)
        {
            if (source == null)
            {
                if (_sampleSource == null)
                {
                    labelSampleOutput.Text = string.Empty;
                    return;
                }
                source = _sampleSource;
            }

            labelSampleOutput.Text = source.GetFormattedFilename(textBoxFilenameStructure.Text, comboBoxSeasonEpisodeIdentifier.SelectedValue.ToString(), checkBoxRemoveYear.Checked);
        }

        private ShowProxy IdentifyWithChildren(string path)
        {
            ScanFolderDialog dlg = new ScanFolderDialog();
            dlg.RootPath = path;
            dlg.ShowDialog();
            return dlg.Root;
        }

        private ShowProxy IdentifyWithChildren(ShowProxy root)
        {
            ScanFolderDialog dlg = new ScanFolderDialog();
            dlg.Root = root;
            dlg.ShowDialog();
            return dlg.Root;
        }

        /// <summary>
        /// Populates the treeListViewBrowser by searching the supplied directory.
        /// </summary>
        /// <param name="path">The path to the directory to search for ShowNodes.</param>
        private void PopulateTreeListViewBrowser(string path)
        {
            Cursor = Cursors.WaitCursor;
            treeListViewBrowser.ClearObjects();
            treeListViewBrowser.ClearCachedInfo();
            _sampleSource = null;
            _rootArray = new ShowProxy[1];

            if (!System.IO.Directory.Exists(path))
            {
                Cursor = Cursors.Default;
                return;
            }

            _root = IdentifyWithChildren(path);
            _rootArray[0] = _root;

            treeListViewBrowser.SetObjects(_rootArray);
            treeListViewBrowser.CanExpandGetter = delegate(object item)
            {
                ShowProxy proxy = item as ShowProxy;
                return proxy.HasChildren;
            };
            treeListViewBrowser.ChildrenGetter = delegate(object item)
            {
                ShowProxy proxy = item as ShowProxy;
                return proxy.Children;
            };

            // Aesthetics
            treeListViewBrowser.ExpandAll();
            treeListViewBrowser.Select();

            for (int i = 0; i < treeListViewBrowser.Items.Count; i++)
            {
                ShowProxy item = treeListViewBrowser.GetItem(i).RowObject as ShowProxy;

                // Get the sample source
                if (_sampleSource == null && !item.HasChildren && item.SeriesTitle != "" && item.SeasonNum >= -1 && item.EpisodeNum > 0)
                {
                    _sampleSource = item;
                }

                // Set this row's checked state
                if (!item.HasChildren && !(item.SeriesTitle == "" || item.SeasonNum < -1 || item.EpisodeNum < 1))
                {
                    treeListViewBrowser.GetItem(i).Checked = true;
                }


            }
            treeListViewBrowser.Sort(0);
            populateSampleOutput();

            Cursor = Cursors.Default;
        }

        /// <summary>
        /// Returns whether a valid value has been placed in textBoxFilenameStructure, defined as whether the identifier is included.
        /// </summary>
        /// <returns>Whether the value of textBoxFilenameStructure is valid.</returns>
        private bool FilenameStructureIsValid()
        {
            return textBoxFilenameStructure.Text.Contains("[IDENTIFIER]");
        }
    }
}
