﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Text.RegularExpressions;

namespace Batru
{
    /// <summary>
    /// A ShowNode representing a file suspected of being an episode of a television series and its identifying details.
    /// </summary>
    public class Episode : ShowNode
    {
        private FileInfo _file;
        
        /// <summary>
        /// Constructor for Episode
        /// </summary>
        /// <param name="file">FileInfo of the file this Episode describes</param>
        public Episode(FileInfo file):
            base() 
        {
            this._file = file;
        }
        
        /// <summary>
        /// Interface for allowing ShowVisitors to operate on this Episode.
        /// </summary>
        /// <param name="visitor">The ShowVisitor to invoke.</param>
        public override void Accept(ShowVisitor visitor)
        {
            visitor.VisitEpisode(this);
        }

        /// <summary>
        /// This Episode's filename
        /// </summary>
        public override string Filename
        {
            get
            {
                string formattedFilename = Regex.Replace(_file.Name, ShowVisitor._filenameElementsToIgnore, "");
                return formattedFilename;
            }
        }

        /// <summary>
        /// The full path to this ShowNode's directory in string form.
        /// </summary>
        public string DirectoryPath
        {
            get
            {
                return _file.DirectoryName;
            }
        }

        /// <summary>
        /// The full path to this ShowNode's file in string form.
        /// </summary>
        public string FullPath
        {
            get
            {
                return _file.FullName;
            }
        }

        /// <summary>
        /// This ShowNode's file's extension in string form.
        /// </summary>
        public string Extension
        {
            get
            {
                return _file.Extension;
            }
        }
    }
}
