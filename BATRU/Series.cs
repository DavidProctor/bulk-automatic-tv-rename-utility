﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Batru
{
    /// <summary>
    /// Represents a whole television series, such as "The Simpsons" as a whole
    /// </summary>
    public class Series
    {
        private List<Dictionary<int, string>> _seasonsAndEpisodes;
        public List<Series> _alternatives;
        private int _altIndex;

        /// <summary>
        /// Constructor; does not add to SeriesFactory._allSeries, so should only be used by SeriesFactory. Others can use SeriesFactory.GetSeriesById() or SeriesFactory.GetSeriesByTitle().
        /// </summary>
        public Series()
        {
            _seasonsAndEpisodes = new List<Dictionary<int, string>>();
            _altIndex = 0;
            this.Verified = false;
            this.IsNotTV = false;
        }

        /// <summary>
        /// The series ID on TheTVDB.com
        /// </summary>
        public int Id
        {
            get;
            set;
        }

        /// <summary>
        /// Whether this Series has had its details downloaded on TheTVDB.com.
        /// </summary>
        public bool Verified
        {
            get;
            set;
        }

        /// <summary>
        /// The name of this series
        /// </summary>
        public string Title
        {
            get;
            set;
        }

        /// <summary>
        /// The number of seasons in this show. If there are specials, they will be counted as a season.
        /// </summary>
        public int Seasons
        {
            get
            {
                return _seasonsAndEpisodes.Count;
            }
        }

        /// <summary>
        /// A boolean value that can indicate if it's fairly clear that this series is not a television show.
        /// </summary>
        public bool IsNotTV
        {
            get;
            set;
        }

        /// <summary>
        /// The number of episodes in the specified season.
        /// </summary>
        /// <param name="season">Which season to check for episode count.</param>
        /// <returns>The number of episodes in the specified season.</returns>
        public int GetEpisodesInSeason(int season)
        {
            if (season < 0)
            {
                season = 1;
            }
            if (season >= _seasonsAndEpisodes.Count)
            {
                return -1;
            }
            return _seasonsAndEpisodes[season].Count + 1;
        }

        /// <summary>
        /// The title of the specified episode.
        /// </summary>
        /// <param name="season">The season of the episode whose title is being searched for.</param>
        /// <param name="episode">The episode number of the episode whose title is being searched for.</param>
        /// <returns>The title of the specified episode, or blank string if there is no such episode.</returns>
        public string GetEpisodeTitle(int season, int episode)
        {
            string episodeTitle;

            // Compensate for miniseries
            if (season == -1)
            {
                season = 1;
            }

            try
            {
                if (_seasonsAndEpisodes[season].TryGetValue(episode, out episodeTitle))
                {
                    return episodeTitle;
                }
                return string.Empty;
            }
            catch (ArgumentOutOfRangeException)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Adds an episode to this series record.
        /// </summary>
        /// <param name="season">The season of the episode to add.</param>
        /// <param name="episode">The episode number within the season of the episode to add.</param>
        /// <param name="name">The title of the episode to add.</param>
        public void AddEpisode(int season, int episode, string name = null)
        {
            // If there's no episode name, use an empty string instead of a null value
            if (name == null)
            {
                name = string.Empty;
            }

            // Ensure that there are enough seasons in _seasonsAndEpisodes to contain this episode's season
            while (_seasonsAndEpisodes.Count <= season || (_seasonsAndEpisodes.Count == season && season == 0))
            {
                _seasonsAndEpisodes.Add(new Dictionary<int,string>());
            }

            // Create space for the episode and place the name
            if (!_seasonsAndEpisodes[season].ContainsKey(episode))
            {
                _seasonsAndEpisodes[season].Add(episode, name);
            }
            else
            {
                _seasonsAndEpisodes[season][episode] = name;
            }
        }

        /// <summary>
        /// Takes a set of search results and records them as alternative series.
        /// </summary>
        /// <param name="alternatives">A list of search results that can act as alternatives to this Series.</param>
        public void SetAlternatives(List<Series> alternatives)
        {
            _alternatives = alternatives;
        }

        /// <summary>
        /// Retrieves the alternative at the current index position.
        /// </summary>
        /// <returns>The currently-selected alternative series, or this series if there is a problem.</returns>
        public Series GetAlternative()
        {
            if (_alternatives == null || _altIndex >= _alternatives.Count || _altIndex < 0)
            {
                return this;
            }
            else
            {
                return _alternatives[_altIndex];
            }
        }

        /// <summary>
        /// Attempts to increment the alternative series index.
        /// </summary>
        /// <returns>Whether a new alternative series was successfully selected.</returns>
        public bool AttemptNextAlternative()
        {
            _altIndex++;
            if (_alternatives == null || _altIndex >= _alternatives.Count)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Moves the alternative series index to the starting position.
        /// </summary>
        public void ResetAlternatives()
        {
            _altIndex = 0;
        }

        /// <summary>
        /// Whether this Series contains a given Series as an alternative.
        /// </summary>
        /// <param name="test">The Series to check this Series for a reference to as an alternative.</param>
        /// <returns>Whether this Series contains the given Series as an alternative.</returns>
        public bool HasAlternative(Series test)
        {
            if (_alternatives == null)
            {
                return false;
            }
            return _alternatives.Contains(test);
        }
    }
}
