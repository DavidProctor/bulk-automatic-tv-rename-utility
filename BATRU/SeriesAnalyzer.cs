﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;

namespace Batru
{
    /// <summary>
    /// Applies details from TheTVDB.com to ShowNodes. Subclass of ShowVisitor.
    /// </summary>
    public class SeriesAnalyzer : ShowVisitor
    {
        private static SeriesAnalyzer _instance;  // For Singleton pattern
        
        /// <summary>
        /// Constructor; privately scoped for Singleton
        /// </summary>
        private SeriesAnalyzer()
        {
        }

        /// <summary>
        /// Returns an instance of DatabaseAnalyzer, creating it if necessary
        /// </summary>
        /// <returns>An instance of DatabaseAnalyzer</returns>
        public static SeriesAnalyzer GetInstance()
        {
            if (_instance == null)
            {
                _instance = new SeriesAnalyzer();
            }
            return _instance;
        }

        /// <summary>
        /// Resolves the aliases of an Episode and sets the episode name.
        /// </summary>
        /// <param name="ep">The episode to operate upon.</param>
        public override void VisitEpisode(Episode ep)
        {
            Dictionary<Series, Certainty> candidateDict = ep.SeriesCandidates;
            List<Series> candidates = candidateDict.Keys.ToList();

            // Loop through each series candidate, prioritizing the highest-certainty candidates
            foreach (KeyValuePair<Series, Certainty> candidate in ep.SeriesCandidates)
            {
                Series theSeries = candidate.Key;

                // Remove any candidate that is labeled "IsNotTV"
                if (theSeries.IsNotTV && candidate.Value != Certainty.UserSupplied)
                {
                    ep.RemoveSeriesCandidate(theSeries);
                }

                // If this series is not verified, search for it
                if (!theSeries.Verified)
                {
                    List<Series> searchResults = SeriesFactory.SearchForShow(theSeries);
                    // If the search finds nothing, this must not be a TV show
                    if (searchResults.Count == 0)
                    {
                        if (candidate.Value == Certainty.UserSupplied)
                        {
                            return;
                        }
                        ep.RemoveSeriesCandidate(theSeries);
                        theSeries.IsNotTV = true;
                        continue;
                    }

                    // Download all detected series; usually 1 and rarely more than 2, so very little overhead here
                    foreach (Series result in searchResults)
                    {
                        SeriesFactory.DownloadSeries(result);
                    }

                    // If we have multiple search results, set them as alternatives
                    if (searchResults.Count > 1)
                    {
                        theSeries.SetAlternatives(searchResults);
                    }
                }

                if (!EpisodeFitsInSeries(ep, theSeries))
                {
                    // This episode number doesn't fit in this series - try alternatives instead
                    while (theSeries.AttemptNextAlternative())
                    {
                        if (!theSeries.Verified)
                        {
                            SeriesFactory.DownloadSeries(theSeries);
                        }
                        if (EpisodeFitsInSeries(ep, theSeries.GetAlternative()))
                        {
                            ep.RemoveSeriesCandidate(theSeries);
                            ep.AddSeriesCandidate(theSeries.GetAlternative(), candidate.Value);
                            theSeries.ResetAlternatives();
                            break;
                        }
                        theSeries.ResetAlternatives();
                    }
                    // Only discard this series candidate if it was not supplied by the user
                    if (candidate.Value != Certainty.UserSupplied)
                    {
                        continue;
                    }
                }

                // Passed all our tests - enhance the certainty
                if (candidate.Value != Certainty.UserSupplied)
                {
                    ep.AddSeriesCandidate(theSeries, Certainty.Verified);
                }
                break;
            }

            ApplyDetails(ep);
        }

        /// <summary>
        /// Not implemented for directories
        /// </summary>
        /// <param name="dir">The Directory to operate upon.</param>
        public override void VisitDirectory(Directory dir)
        {
            return;
        }

        /// <summary>
        /// Whether or not the given Episode's season and episode numbers fit within the given Series.
        /// </summary>
        /// <param name="theEpisode">The Episode being checked for fitting within the given Series.</param>
        /// <param name="theSeries">The Series beign checked for capacity for the given Episode.</param>
        /// <returns>Whether the given Episode fits within the given Series.</returns>
        private bool EpisodeFitsInSeries(Episode theEpisode, Series theSeries)
        {
            if (theEpisode == null || theSeries == null || theEpisode.SeasonNum > theSeries.Seasons || theEpisode.EpisodeNum > theSeries.GetEpisodesInSeason(theEpisode.SeasonNum))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// If this node's likeliest series has been verified by the database, applies its details. If not, flags this series as not likely to be a TV show.
        /// </summary>
        /// <param name="node"></param>
        public void ApplyDetails(ShowNode node)
        {
            // Call this method on all children
            if (node is Directory)
            {
                Directory dir = node as Directory;
                foreach (ShowNode thisNode in dir.contents)
                {
                    ApplyDetails(thisNode);
                }
                return;
            }

            // Fail if there are no Series candidates
            Series theSeries = node.LikeliestSeries;
            if (theSeries == null)
            {
                return;
            }

            // If this Series has been verified, apply episode titles
            if (theSeries.Verified)
            {
                // Check for miniseries
                if (node.SeasonNum == -2 && (theSeries.Seasons == 1 || theSeries.Seasons == 2))
                {
                    node.SetSeason(-1, Certainty.Verified);
                }
                node.SetEpisodeTitle(theSeries.GetEpisodeTitle(node.SeasonNum, node.EpisodeNum), Certainty.Verified);
            }
        }

    }
}
