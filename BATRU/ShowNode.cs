﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Batru
{
    /// <summary>
    /// Indicates the level of priority to give an associated value.
    /// </summary>
    public enum Certainty
    {
        None,
        Weak,
        Strong,
        Complete,
        Verified,
        UserSupplied
    }
    
    /// <summary>
    /// Represents a file that is likely to be an episode of some TV show or a directory containing episodes.
    /// </summary>
    public abstract class ShowNode
    {
        protected Dictionary<Series, Certainty> seriesCandidates;
        protected KeyValuePair<int, Certainty> season;
        protected KeyValuePair<int, Certainty> episode;
        protected KeyValuePair<string, Certainty> episodeTitle;
                
        /// <summary>
        /// Base ShowNode constructor
        /// </summary>
        public ShowNode()
        {
            // Initialize collections
            this.seriesCandidates = new Dictionary<Series, Certainty>();
            this.season = new KeyValuePair<int, Certainty>();
            this.episode = new KeyValuePair<int, Certainty>();
        }

        /// <summary>
        /// Allows a ShowVisitor to operate on this ShowNode.
        /// </summary>
        /// <param name="visitor">The ShowVisitor to invoke.</param>
        public abstract void Accept(ShowVisitor visitor);
        
        /// <summary>
        /// Connects to a databse, downloads details, and applies them to this node and all its children.
        /// </summary>
        public void DownloadDetails()
        {
            this.Accept(SeriesAnalyzer.GetInstance());
        }

        /// <summary>
        /// Adds a candidate Series to this ShowNode, if it is not already being considered.
        /// </summary>
        /// <param name="seriesCandidate">The Series instance to add to this ShowNode.</param>
        /// <param name="candidateCertainty">The certainty of this Series possibility.</param>
        public virtual void AddSeriesCandidate(Series seriesCandidate, Certainty candidateCertainty)
        {
            // No need to add this candidate if we know it's wrong
            if (candidateCertainty == Certainty.None || seriesCandidate == null || seriesCandidate.IsNotTV)
            {
                return;
            }

            // If this is replacing a UserSupplied value, demote the old value
            List<Series> allCandidates = seriesCandidates.Keys.ToList<Series>();
                //new List<Series>(seriesCandidates.Keys);
            foreach (Series thisSeries in allCandidates)
            {
                if (seriesCandidates[thisSeries] == Certainty.UserSupplied)
                {
                    if (thisSeries.Verified)
                    {
                        seriesCandidates[thisSeries] = Certainty.Verified;
                    }
                    else
                    {
                        seriesCandidates[thisSeries] = Certainty.Complete;
                    }
                }
            }

            // Check if this series is already in this ShowNode
            if (this.seriesCandidates.ContainsKey(seriesCandidate))
            {
                if (candidateCertainty > this.seriesCandidates[seriesCandidate])
                {
                    this.seriesCandidates[seriesCandidate] = candidateCertainty;
                }
            }
            else
            {
                this.seriesCandidates.Add(seriesCandidate, candidateCertainty);
            }

            // Remove the episode title
            if (candidateCertainty > EpisodeTitlePair.Value)
            {
                episodeTitle = new KeyValuePair<string, Certainty>(string.Empty, Certainty.None);
            }
        }

        /// <summary>
        /// Adds a candidate Series to this ShowNode, if it is not already being considered.
        /// </summary>
        /// <param name="titleCandidate">The candidate series title to add.</param>
        /// <param name="candidateCertainty">Certainty level of this candidate series title.</param>
        public void AddSeriesCandidate(string titleCandidate, Certainty candidateCertainty)
        {
            Series candidate = SeriesFactory.GetSeries(titleCandidate);
            AddSeriesCandidate(candidate, candidateCertainty);
        }

        /// <summary>
        /// Removes a given series from this ShowNode's seriesCandidates dictionary.
        /// </summary>
        /// <param name="seriesToRemove">The Series to remove from seriesCandidates.</param>
        public virtual Certainty RemoveSeriesCandidate(Series seriesToRemove)
        {
            if (seriesCandidates.ContainsKey(seriesToRemove))
            {
                Certainty output = seriesCandidates[seriesToRemove];
                seriesCandidates.Remove(seriesToRemove);
                this.episodeTitle = new KeyValuePair<string, Certainty>(string.Empty, Certainty.None);
                return output;
            }
            return Certainty.None;
        }

        /// <summary>
        /// Sets the season number of this ShowNode, if the new value is more certain than the old value (or equal). Returns whether the change was completed.
        /// </summary>
        /// <param name="seasonToSet">The number to set the ShowNode's season to.</param>
        /// <param name="certaintyToSet">The certainty of the new value.</param>
        /// <returns>Whether the ShowNode's season number was changed.</returns>
        public bool SetSeason(int seasonToSet, Certainty certaintyToSet)
        {
            // No need to add this candidate if we know it's wrong
            if (certaintyToSet == Certainty.None)
            {
                return false;
            }

            // Use the value with the greater certainty. If certainty is equal, don't change.
            if (certaintyToSet >= this.season.Value)
            {
                this.season = new KeyValuePair<int, Certainty>(seasonToSet, certaintyToSet);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets the episode number of this ShowNode within the season, if the new value is more certain than the old value (or equal). Returns whether the change was completed.
        /// </summary>
        /// <param name="episodeToSet">The number to set the ShowNode's episode to.</param>
        /// <param name="certaintyToSet">The certianty of the new Episode value.</param>
        /// <returns>Whether the ShowNode's episode number was changed.</returns>
        public bool SetEpisode(int episodeToSet, Certainty certaintyToSet)
        {
            // No need to add this candidate if we know it's wrong
            if (certaintyToSet == Certainty.None)
            {
                return false;
            }

            // Use the value with the greater certainty. If certainty is equal, change it (in order to allow the user to change it twice).
            if (certaintyToSet >= this.episode.Value)
            {
                this.episode = new KeyValuePair<int, Certainty>(episodeToSet, certaintyToSet);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Set the episode title of this ShowNode if the new value is more certain than the old value (or equal). Returns whether the title was changed.
        /// </summary>
        /// <param name="titleToSet">The title to set for the ShowNode's episode.</param>
        /// <param name="certaintyToSet">The certainty of the new Episode value.</param>
        /// <returns>Whether the ShowNode's episode number was changed.</returns>
        public virtual bool SetEpisodeTitle(string titleToSet, Certainty certaintyToSet)
        {
            // No need to add this candidate if we know it's wrong
            if (certaintyToSet == Certainty.None || string.IsNullOrEmpty(titleToSet))
            {
                return false;
            }

            // Use the value with the greater certainty. change it (in order to allow the user to change it twice).
            if (certaintyToSet >= this.episodeTitle.Value)
            {
                this.episodeTitle = new KeyValuePair<string, Certainty>(titleToSet, certaintyToSet);
                return true;
            }

            return false;
        }

        /// <summary>
        /// The name of this file on the disk
        /// </summary>
        public abstract string Filename
        {
            get;
        }

        /// <summary>
        /// The title of the likeliest Series of this ShowNode. Only the end user should use Set - use AddSeriesCandidate for all other contexts.
        /// </summary>
        public virtual string LikeliestSeriesTitle
        {
            get
            {
                if (LikeliestSeriesPair.Key != null)
                {
                    return LikeliestSeriesPair.Key.Title;
                }
                return string.Empty;
            }
        }

        /// <summary>
        /// The likeliest Series of this ShowNode.
        /// </summary>
        public Series LikeliestSeries
        {
            get
            {
                return this.LikeliestSeriesPair.Key;
            }
        }

        /// <summary>
        /// The likeliest Series of this ShowNode, in a KeyValuePair with its certainty.
        /// </summary>
        public KeyValuePair<Series, Certainty> LikeliestSeriesPair
        {
            get
            {
                KeyValuePair<Series, Certainty> mostCertainSeries = new KeyValuePair<Series, Certainty>(null, Certainty.None);
                foreach (KeyValuePair<Series, Certainty> entry in this.seriesCandidates)
                {
                    if (entry.Value == Certainty.UserSupplied)
                    {
                        return entry;
                    }
                    if (entry.Value > mostCertainSeries.Value)
                    {
                        mostCertainSeries = entry;
                    }
                }

                return mostCertainSeries;
            }
        }

        /// <summary>
        /// Returns the dictionary containing all series candidates, ordered from highest certainty to lowest
        /// </summary>
        public Dictionary<Series, Certainty> SeriesCandidates
        {
            get
            {
                return this.seriesCandidates.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
            }
        }

        /// <summary>
        /// The season number of this ShowNode. Only the end user should use Set - use SetSeasonCandidate for all other contexts.
        /// </summary>
        public virtual int SeasonNum
        {
            get
            {
                return this.season.Key;
            }
            set
            {
                this.SetSeason(value, Certainty.UserSupplied);
            }
        }

        /// <summary>
        /// The season number of this ShowNode, in a KeyValuePair with its certainty.
        /// </summary>
        public virtual KeyValuePair<int, Certainty> SeasonPair
        {
            get
            {
                return this.season;
            }
        }

        /// <summary>
        /// The episode number of this ShowNode. Only the end user should use Set - use SetEpisode for all other contexts.
        /// </summary>
        public virtual int EpisodeNum
        {
            get
            {
                return this.episode.Key;
            }
            set
            {
                this.SetEpisode(value, Certainty.UserSupplied);
            }
        }

        /// <summary>
        /// The episode number of this ShowNode, in a KeyValuePair with its certainty.
        /// </summary>
        public KeyValuePair<int, Certainty> EpisodePair
        {
            get
            {
                return this.episode;
            }
        }

        /// <summary>
        /// The episode title of this ShowNode. Only the end user should use Set - use SetEpisodeTitle for all other contexts.
        /// </summary>
        public string EpisodeTitle
        {
            get
            {
                return this.episodeTitle.Key;
            }
            set
            {
                this.SetEpisodeTitle(value, Certainty.UserSupplied);
            }
        }

        /// <summary>
        /// The episode title of this ShowNode, in a KeyValuePair with its certainty.
        /// </summary>
        public KeyValuePair<string, Certainty> EpisodeTitlePair
        {
            get
            {
                return this.episodeTitle;
            }
        }

        /// <summary>
        /// Ad-hoc debug string
        /// </summary>
        public virtual string Debug
        {
            get;
            set;
        }
    }
}
