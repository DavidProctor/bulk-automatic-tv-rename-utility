﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Text.RegularExpressions;

namespace Batru
{
    /// <summary>
    /// A ShowNode representing a directory, as well as summary identifying information about the contianed series, season number, and episode number.
    /// </summary>
    public class Directory : ShowNode
    {
        // Set file types to consider
        private static string[] _extensions = new string[] {
                    ".avi",
                    ".mkv",
                    ".mp4",
                    ".mpeg",
                    ".mpg",
                    ".wmv",
                    ".flv"
                };

        private DirectoryInfo _directory;
        public List<ShowNode> contents;
        public bool isConsistent;

        /// <summary>
        /// Constructor for Directories. Initializes fields, populates contents, and attempts to determine ShowNode details.
        /// </summary>
        /// <param name="rootPath">The path of this Directory.</param>
        public Directory(string rootPath):
            base()
        {
            this.isConsistent = true;

            // Initialize fields
            _directory = new DirectoryInfo(rootPath);
            contents = new List<ShowNode>();
            
            // Create directory structure
            try
            {
                this.PopulateContents();
            }
            catch (UnauthorizedAccessException)
            {
                // No implementation; the empty directory will be destroyed
            }
        }

        /// <summary>
        /// Interface for allowing ShowVisitors to operate on this Directory.
        /// </summary>
        /// <param name="visitor">The ShowVisitor to invoke.</param>
        public override void Accept(ShowVisitor visitor)
        {
            foreach (ShowNode node in this.contents)
            {
                node.Accept(visitor);
            }
            visitor.VisitDirectory(this);
        }

        /// <summary>
        /// Recursively populate the contents of this folder and its children at all levels
        /// </summary>
        internal void PopulateContents()
        {
            // Add TV episode files in the current folder to this collection
            foreach (FileInfo file in _directory.GetFiles())
            {
                if (_extensions.Contains(file.Extension) && file.Name.Substring(0, 2) != "._")
                {
                    Episode thisEpisode = new Episode(file);
                    this.contents.Add(thisEpisode);
                }
            }

            // Recursively create a tree structure. 
            foreach (DirectoryInfo dir in _directory.GetDirectories())
            {
                Directory newDirectory = new Directory(dir.FullName);

                // Only add directories that have potential episodes in them
                if (newDirectory.contents.Count > 0)
                {
                    this.contents.Add(newDirectory);
                }
            }
        }

        /// <summary>
        /// This directory's formatted name
        /// </summary>
        public override string Filename
        {
            get 
            {
                string formattedFilename = Regex.Replace(_directory.Name, ShowVisitor._filenameElementsToIgnore, "");
                return formattedFilename;
            }
        }

        ///// <summary>
        ///// Replaces the current series candidates with a supplied list.
        ///// </summary>
        ///// <param name="candidates">A List of Series containing the new Series candidates.</param>
        ///// <param name="consistentSeries">A single series that has been determined to be consistent among all of this Directory's candidates.</param>
        //public void SetSeriesCandidates(List<Series> candidates, Series consistentSeries = null)
        //{
        //    if (candidates == null)
        //    {
        //        return;
        //    }

        //    this.seriesCandidates = new Dictionary<Series, Certainty>();
        //    foreach (Series thisSeries in candidates)
        //    {
        //        this.AddSeriesCandidate(thisSeries, Certainty.Strong);
        //    }

        //    if (consistentSeries != null)
        //    {
        //        if (consistentSeries.Verified)
        //        {
        //            this.AddSeriesCandidate(consistentSeries, Certainty.Verified);
        //        }
        //        else
        //        {
        //            this.AddSeriesCandidate(consistentSeries, Certainty.Complete);
        //        }
        //        //this.AddSeriesCandidate(consistentSeries, Certainty.Complete);
        //    }
        //}

        /// <summary>
        /// Adds a new series candidate to this Directory and cascades to all children.
        /// </summary>
        /// <param name="seriesCandidate">The series candidiate to add.</param>
        /// <param name="candidateCertainty">The certainty of the series candidate.</param>
        public override void AddSeriesCandidate(Series seriesCandidate, Certainty candidateCertainty)
        {

            base.AddSeriesCandidate(seriesCandidate, candidateCertainty);
            foreach (ShowNode node in contents)
            {
                node.AddSeriesCandidate(seriesCandidate, candidateCertainty);
            }
        }

        /// <summary>
        /// Removes a supplied Series from the series candidates of all children.
        /// </summary>
        /// <param name="seriesToRemove">The series to remove from this Directory.</param>
        /// <returns>The highest certainty that was associated with the removed Series, or Certainty.None if it was not found.</returns>
        public override Certainty RemoveSeriesCandidate(Series seriesToRemove)
        {
            Certainty highestCertainty = Certainty.None;
            foreach (ShowNode node in contents)
            {
                Dictionary<Series, Certainty> candidates = node.SeriesCandidates;
                if (!candidates.ContainsKey(seriesToRemove))
                {
                    continue;
                }
                Certainty thisCertainty = candidates[seriesToRemove];
                if (thisCertainty > highestCertainty)
                {
                    highestCertainty = thisCertainty;
                }
                node.RemoveSeriesCandidate(seriesToRemove);
            }
            return highestCertainty;
        }

        /// <summary>
        /// Replaces a given series candidate with a given replacement in all children at the original's Certainty level.
        /// </summary>
        /// <param name="original">The series candidate to remove.</param>
        /// <param name="replacement">The series candidate to apply in the place of the removed candidate.</param>
        public void ReplaceSeriesCandidate(Series original, Series replacement)
        {
            foreach (ShowNode node in contents)
            {
                Certainty theCertainty = node.RemoveSeriesCandidate(original);
                node.AddSeriesCandidate(replacement, theCertainty);
            }
        }

        /// <summary>
        /// Determines the likeliest season number for this Directory if it is consistent; returns the appropriate replacement value otherwise. Set defaults to base set.
        /// </summary>
        public override int SeasonNum
        {
            get
            {
                // Find a sample SeasonNum
                int sample;
                if (this.contents.Count > 0)
                {
                    sample = this.contents[0].SeasonNum;
                }
                else
                {
                    // No Episodes or Directories found in this Directory; return "unknown".
                    return -2;
                }

                // Loop through contents and check for consistent season number
                foreach (ShowNode node in contents)
                {
                    int check = node.SeasonNum;
                    if (check != sample)
                    {
                        if (check == -3 || check >= 0)
                        {
                            return -3;
                        }
                        else
                        {
                            return -2;
                        }
                    }
                }

                return sample;
            }
            set
            {
                // Since this should only be called by the end-user, sets all contents SeasonNum to given value
                this.SetSeason(value, Certainty.UserSupplied);
                foreach (ShowNode node in contents)
                {
                    node.SeasonNum = value;
                }
            }
        }

        /// <summary>
        /// Determines the likeliest episode number for this Directory if it is consistent, and substitutes replacement numbers otherwise. Set remains at the base.
        /// </summary>
        public override int EpisodeNum
        {
            get
            {
                // Find a sample EpisodeNum
                int sample;
                if (this.contents.Count > 0)
                {
                    sample = this.contents[0].EpisodeNum;
                }
                else
                {
                    // No Episodes or Directories found in this Directory; return "unknown".
                    return 0;
                }

                // Loop through contents and check for consistent season number
                foreach (ShowNode node in contents)
                {
                    int check = node.EpisodeNum;
                    if (check != sample)
                    {
                        if (check == -1 || check >= 1)
                        {
                            return -1;
                        }
                        else
                        {
                            return 0;
                        }
                    }
                }

                return sample;
            }
            set
            {
                // Since this should only be called by the end-user, sets all contents EpisodeNum to given value
                this.SetEpisode(value, Certainty.UserSupplied);
                foreach (ShowNode node in contents)
                {
                    node.EpisodeNum = value;
                }
            }
        }

        /// <summary>
        /// Attempts to set the episode title of all children.
        /// </summary>
        /// <param name="titleToSet">The title to set for all children.</param>
        /// <param name="certaintyToSet">The certianty to set children's titles to.</param>
        /// <returns>Whether the attempt was successful.</returns>
        public override bool SetEpisodeTitle(string titleToSet, Certainty certaintyToSet)
        {
            bool success = base.SetEpisodeTitle(titleToSet, certaintyToSet);
            if (!success) { return success; }
            foreach (ShowNode node in this.contents)
            {
                node.SetEpisodeTitle(titleToSet, certaintyToSet);
                if (!success) { return success; }
            }
            return success;
        }

        /// <summary>
        /// The title of this Directory's likeliest Series candidate.
        /// </summary>
        public override string LikeliestSeriesTitle
        {
            get
            {
                this.Accept(ContextAnalyzer.GetInstance());
                if (this.isConsistent)
                {
                    return base.LikeliestSeriesTitle;
                }
                return "[Mixed]";
            }
        }

        /// <summary>
        /// The full path to this Directory in a string.
        /// </summary>
        public string FullPath
        {
            get
            {
                return _directory.FullName;
            }
        }
    }
}
