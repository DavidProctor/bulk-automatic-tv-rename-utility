﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace Batru
{
    public enum RelocationOptions
    {
        Copy,
        Move,
        InPlace
    }

    /// <summary>
    /// A collection of settings for organizing and re-naming Episodes.
    /// </summary>
    public class FileOrganizationSettings
    {
        private static FileOrganizationSettings _instance;

        private FileOrganizationSettings() { }

        public static FileOrganizationSettings GetInstance()
        {
            if (_instance == null)
            {
                _instance = new FileOrganizationSettings();
            }
            return _instance;
        }

        /// <summary>
        /// Whether to create a folder for each series.
        /// </summary>
        public bool FolderForSeries
        {
            get;
            set;
        }

        /// <summary>
        /// Whether to create a folder for each season.
        /// </summary>
        public bool FolderForSeason
        {
            get;
            set;
        }

        ///// <summary>
        ///// Whether to create a separate folder for specials.
        ///// </summary>
        //public bool FolderForSpecials
        //{
        //    get;
        //    set;
        //}

        /// <summary>
        /// Whether to remove year suffixes from show titles.
        /// </summary>
        public bool RemoveYearFromTitles
        {
            get;
            set;
        }

        /// <summary>
        /// Whether to relocate files by copying, moving, or leaving them in place.
        /// </summary>
        public RelocationOptions RelocationOption
        {
            get;
            set;
        }

        /// <summary>
        /// The folder to relocate files to.
        /// </summary>
        public DirectoryInfo DestinationFolder
        {
            get;
            set;
        }

        /// <summary>
        /// The string that will be used in the Regex for renaming files.
        /// </summary>
        public string FilenameStructure
        {
            get;
            set;
        }

        /// <summary>
        /// The style that will be used for identifying episode and season numbers.
        /// </summary>
        public string IdentifierStyle
        {
            get;
            set;
        }

        /// <summary>
        /// Sets the folder that the selected nodes will be moved into.
        /// </summary>
        /// <param name="path">The path of the destination folder as a string.</param>
        public void SetDestinationFolder(string path)
        {
            DestinationFolder = new DirectoryInfo(path);
        }
    }
}
