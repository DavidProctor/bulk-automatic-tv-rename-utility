﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Text.RegularExpressions;

namespace Batru
{
    /// <summary>
    /// Static class that organizes files based on supplied FileOrganizationSettings
    /// </summary>
    public static class FileOrganizer
    {
        private static char[] allInvalidChars = new char[] { '\\', '/', ':', '*', '?', '"', '<', '>', '|' };

        /// <summary>
        /// Given an Episode and a FileOrganizationSettings, organizes the episode file according to the settings.
        /// </summary>
        /// <param name="ep">The Episode referring to the file to be organized.</param>
        /// <param name="settings">The FileOrganizationSettings indicating how to organize the file.</param>
        public static void OrganizeEpisode(Episode ep, FileOrganizationSettings settings)
        {
            // Establish the new filename first
            string newFilename = GetFormattedFilename(ep, settings.FilenameStructure, settings.IdentifierStyle, settings.RemoveYearFromTitles);

            // Establish the new series title and remove year suffix if necessary
            string seriesTitle = ep.LikeliestSeriesTitle;
            if (settings.RemoveYearFromTitles)
            {
                seriesTitle = GetStringWithoutYearSuffix(seriesTitle);
            }

            // Set the location to move the file to to the default
            string newFolder = ep.DirectoryPath;

            // If we're not going to leave the file in place, find the target directory and create it if necessary
            if (settings.RelocationOption == RelocationOptions.Copy || settings.RelocationOption == RelocationOptions.Move)
            {
                newFolder = settings.DestinationFolder.FullName;

                // Set up a folder for the series
                if (settings.FolderForSeries)
                {
                    string seriesFolderName = allInvalidChars.Aggregate(seriesTitle, (current, c) => current.Replace(c.ToString(), string.Empty));
                    newFolder = newFolder + "\\" + seriesFolderName;
                }

                //// Set up a folder for specials
                //if (ep.SeasonNum == 0 && settings.FolderForSpecials)
                //{
                //    newFolder = newFolder + "\\Specials";
                //}
                //else 

                // Do nothing if this episode is part of a miniseries
                if (ep.SeasonNum == -1)
                {

                }
                // Set up a folder for any other season
                else if (settings.FolderForSeason)
                {
                    newFolder = newFolder + "\\Season " + ep.SeasonNum;
                }

                // Create folder if necessary
                if (!System.IO.Directory.Exists(newFolder))
                {
                    System.IO.Directory.CreateDirectory(newFolder);
                }
            }

            // Set up full filename of new file
            string newFullPath = newFolder + "\\" + newFilename;

            // Perform the specified organization action
            try
            {
                if (settings.RelocationOption == RelocationOptions.Copy)
                {
                    File.Copy(ep.FullPath, newFullPath);
                }
                else if (settings.RelocationOption == RelocationOptions.Move)
                {
                    File.Move(ep.FullPath, newFullPath);
                }
                else if (settings.RelocationOption == RelocationOptions.InPlace)
                {
                    File.Move(ep.FullPath, newFullPath);
                }
            }
            // Throw a custom exception if there is a filename conflict
            catch (IOException)
            {
                throw new FileConflictException(newFilename, newFolder);
            }
        }

        /// <summary>
        /// Given a ShowNode, a filename format, and whether to remove the year from the filename, returns the appropriate filename.
        /// </summary>
        /// <param name="source">The ShowNode to create a filename from.</param>
        /// <param name="format">The pattern to substitute this ShowNode's details into.</param>
        /// <param name="removeYear">Whether to remove year suffixes from the filename.</param>
        /// <returns>A formatted filename for the supplied ShowNode.</returns>
        public static string GetFormattedFilename(Episode source, string format, string identifierStyle, bool removeYear)
        {
            string title = source.LikeliestSeriesTitle;

            if (removeYear)
            {
                title = GetStringWithoutYearSuffix(title);
            }
            format = format.Replace("[SERIES]", title);

            string identifier;

            // Compensate for miniseries
            if (source.SeasonNum == -1)
            {
                identifier = "Episode [EPISODE]";
            }
            // Compensate for specials
            else if (source.SeasonNum == 0)
            {
                identifier = "Special";
            }
            // Otherwise, use the provided identifier style
            else
            {
                if (source.LikeliestSeries.Seasons >= 10)
                {
                    // If this series has more than 10 seasons, put a leading zero on the season number
                    identifier = identifierStyle.Replace("[SEASON]", source.SeasonNum.ToString("00"));
                }
                else
                {
                    identifier = identifierStyle.Replace("[SEASON]", source.SeasonNum.ToString());
                }
            }
            identifier = identifier.Replace("[EPISODE]", source.EpisodeNum.ToString("00"));
            format = format.Replace("[IDENTIFIER]", identifier);
            
            format = format.Replace("[EPISODE_TITLE]", source.EpisodeTitle);
            format += source.Extension;

            // Remove invalid characters
            format = format.Replace('/', '-');
            format = Path.GetInvalidFileNameChars().Aggregate(format, (current, c) => current.Replace(c.ToString(), string.Empty));

            return format;
        }

        /// <summary>
        /// Returns the supplied string without a year suffix, if one exists.
        /// </summary>
        /// <param name="source">The supplied string to remove a year suffix from.</param>
        /// <returns>The supplied string without a year suffix.</returns>
        public static string GetStringWithoutYearSuffix(string source)
        {
            Regex reg = new Regex(@" \(\d\d\d\d\)$");
            return reg.Replace(source, string.Empty);
        }

        /// <summary>
        /// Deletes the file at the given location, if it exists.
        /// </summary>
        /// <param name="fullPath">The full path to the file to delete.</param>
        public static void DeleteFile(string fullPath)
        {
            if (File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
        }

        /// <summary>
        /// If RelocationOptions.Move is selected, checks if the directory is empty and deletes it if so.
        /// </summary>
        /// <param name="dir">The directory to organize.</param>
        /// <param name="settings">The FileOrganizationSettings to use for organization.</param>
        public static void OrganizeDirectory(Directory dir, FileOrganizationSettings settings)
        {
            // Do nothing if we are not moving files
            if (settings.RelocationOption != RelocationOptions.Move)
            {
                return;
            }

            // Check if the directory is empty and delete if so
            string path = dir.FullPath;
            if (!System.IO.Directory.EnumerateFileSystemEntries(path).Any())
            {
                System.IO.Directory.Delete(path);
            }
        }
    }
}
