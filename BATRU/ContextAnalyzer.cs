﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Batru
{
    /// <summary>
    /// Subcalss of ShowVisitor that attempts to draw Series, season number, and episode number details by combining information from Directories and separate Episodes.
    /// </summary>
    public class ContextAnalyzer : ShowVisitor
    {
        private static ContextAnalyzer _instance;  // For Singleton pattern

        /// <summary>
        /// Returns the sole instance of ContextAnalyzer, creating it if necessary. This is the interface for the Singleton pattern.
        /// </summary>
        /// <returns>The sole instance of ContextAnalyzer</returns>
        public static ContextAnalyzer GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ContextAnalyzer();
            }
            return _instance;
        }

        /// <summary>
        /// Interface for operating on Episodes. No implementation.
        /// </summary>
        /// <param name="ep">The Episode to operate on.</param>
        public override void VisitEpisode(Episode ep)
        {
            return;
        }

        /// <summary>
        /// Gathers details by comparing the contents of directories, and offers show, season, and episode candidates to children.
        /// </summary>
        /// <param name="dir">The Directory to operate on.</param>
        public override void VisitDirectory(Directory dir)
        {
            Series seriesContained = null;
            List<Series> allCandidates = new List<Series>();

            // Loop through the Directory's contents and determine if there is a consistent series and whether each episode fits in it.
            for (int i = 0; i < dir.contents.Count; i++)
            {
                Series thisSeries = dir.contents[i].LikeliestSeries;

                // Create a list of all series candidates in the Directory
                if (!allCandidates.Contains(thisSeries))
                {
                    allCandidates.Add(thisSeries);
                }

                // If this is the first Series we've found in the Directory, note it for comparison with other nodes
                if (seriesContained == null)
                {
                    seriesContained = thisSeries;
                }

                if (seriesContained != thisSeries)
                {
                    // thisSeries is different because it is an alternative to the earlier nodes' Series. This probably means that it is a different 
                    // show with the same name (ex. "Archer" vs. "Archer (2009)"), so change all directory contents and start over.
                    if (thisSeries == seriesContained.GetAlternative())
                    {
                        dir.ReplaceSeriesCandidate(seriesContained, seriesContained.GetAlternative());
                        seriesContained = null;
                        i = -1;
                        continue;
                    }
                    // Same as above, in opposite direction
                    else if (seriesContained == thisSeries.GetAlternative())
                    {
                        dir.ReplaceSeriesCandidate(thisSeries, thisSeries.GetAlternative());
                        seriesContained = null;
                        i = -1;
                        continue;
                    }
                    // Otherwise, the contents of this folder are simply inconsistent and we can stop analyzing
                    else
                    {
                        dir.isConsistent = false;
                        AddDetailsToChildren(dir);
                        return;
                    }
                }
            }

            // Add the appropriate details to the child nodes
            if (!SeriesFactory.OfflineMode)
            {
                SeriesAnalyzer.GetInstance().ApplyDetails(dir);
            }
        }

        /// <summary>
        /// Checks that a list of ShowNodes contains only a single Series, and returns that Series
        /// </summary>
        /// <param name="contents">The composite of ShowNodes to check for consistency</param>
        /// <param name="seriesContained">A reference to the consistent Series, or null if the series is inconsistent</param>
        /// <returns>Whether the contents of this list have a consistent Series.</returns>
        public bool ContainsSingleSeries(List<ShowNode> contents, out Series seriesContained)
        {
            // Candidate series is the series of the first detected ShowNode. Return false if there are no contents.
            Series candidateSeries;
            if (contents.Count > 0)
            {
                candidateSeries = contents[0].LikeliestSeries;
            }
            else
            {
                seriesContained = null;
                return false;
            }

            foreach (ShowNode node in contents)
            {
                if (node.LikeliestSeries != candidateSeries)
                {
                    seriesContained = null;
                    return false;
                }
            }

            seriesContained = candidateSeries;
            if (seriesContained == null) return false;
            return true;
        }

        /// <summary>
        /// Adds a Directory's likeliest series, season, and episode to all its children recursively
        /// </summary>
        /// <param name="dir">The Directory to modify the children of</param>
        /// <param name="addSeries">Whether to add the series to the children (default true)</param>
        /// <param name="addSeason">Whether to add the season to the children (default true)</param>
        /// <param name="addEpisode">Whether to add the episode to the children (default false)</param>
        private void AddDetailsToChildren(Directory dir, bool addSeries = true, bool addSeason = false, bool addEpisode = false)
        {
            // Only one call to likeliest detail functions
            Series series = dir.LikeliestSeries;
            int season = dir.SeasonNum;
            int episode = dir.EpisodeNum;

            // Don't proceed if there is no good guess for any of the details
            if (series == null && season == -2 && (!addEpisode || (addEpisode && episode == -1) ))
            {
                return;
            }

            KeyValuePair<Series, Certainty> seriesCandidate = new KeyValuePair<Series, Certainty>();
            KeyValuePair<int, Certainty> seasonCandidate = new KeyValuePair<int,Certainty>();
            KeyValuePair<int, Certainty> episodeCandidate = new KeyValuePair<int,Certainty>();

            if (addSeries) { seriesCandidate = dir.LikeliestSeriesPair; }
            if (addSeason) { seasonCandidate = dir.SeasonPair; }
            if (addEpisode) { episodeCandidate = dir.EpisodePair; }

            foreach (ShowNode node in dir.contents)
            {
                // Add details to each node as required
                if (addSeries) { node.AddSeriesCandidate(seriesCandidate.Key, seriesCandidate.Value); }
                if (addSeason) { node.SetSeason(seasonCandidate.Key, seasonCandidate.Value); }
                if (addEpisode) { node.SetEpisode(episodeCandidate.Key, episodeCandidate.Value); }

                // Recursively add details to child directories
                if (node is Directory)
                {
                    AddDetailsToChildren(node as Directory);
                }
            }
        }
    }
}
