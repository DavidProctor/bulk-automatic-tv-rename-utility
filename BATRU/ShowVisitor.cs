﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Batru
{
    /// <summary>
    /// An abstract class that defines the interface for the Visitor pattern, described in the book Design Patterns. Encapsulates detail identification behaviour so that it doesn't need to be mixed into ShowNode objects directly.
    /// </summary>
    public abstract class ShowVisitor
    {
        // Regular expressions for keywords
        public static string[] seasonKeywords = new string[]{
                @"season \d+",
                @"series \d+",
                @"year \d+"
            };
        public static string[] episodeKeywords = new string[]{
                @"episode \d+",
                @"part \d+",
                @"chapter \d+",
                @"session \d+",
                @"week \d+",
                @"ep \d+",
                @"ep\d+"
            };

        // Regular expressions for analyzing filenames
        public static string _episodeIdentifierPattern = @"\b(([Ss]eason|SEASON).?\d?\d.?([Ee]pisode|EPISODE).?\d?\d)|([\[(]?[Ss]? ?\d\d? ?([exEX-_.]|EP) ?\d\d?[\])]?)\b";
        public static char[] _episodeIdentifierSplitters = new char[] { 'e', 'x', 'E', 'X', '-', '_', 'w', 'W' };
        public static string _filenameElementsToIgnore = @"[ _\,\.\(\)\[\]\-](ac3|dts|custom|dc|divx|divx5|dsr|dsrip|dutch|dvd|dvdrip|dvdscr|dvdscreener|screener|dvdivx|cam|fragment|fs|hdtv|hdrip|hdtvrip|internal|limited|multisubs|ntsc|ogg|ogm|pal|pdtv|proper|repack|rerip|retail|cd[1-9]|r3|r5|bd5|se|svcd|swedish|german|read.nfo|nfofix|unrated|ws|telesync|ts|telecine|tc|brrip|bdrip|480p|480i|576p|576i|720p|720i|1080p|1080i|hrhd|hrhdtv|hddvd|bluray|x264|h264|xvid|xvidvd|xxx|www.www|\[.*\])([ _\,\.\(\)\[\]\-]|$)";

        /// <summary>
        /// Interface for implementing behaviours on Episode objects
        /// </summary>
        /// <param name="ep">The Episode to have an added behaviour</param>
        public abstract void VisitEpisode(Episode ep);

        /// <summary>
        /// Interface for implementing behaviours on Directory objects
        /// </summary>
        /// <param name="dir">The Directory to have an added behaviour</param>
        public abstract void VisitDirectory(Directory dir);
    }
}
