﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Text.RegularExpressions;
using System.Globalization;

namespace Batru
{
    /// <summary>
    /// Subclass of ShowVisitor that extracts series, season number, and episode number information from filenames.
    /// </summary>
    public class FilenameAnalyzer: ShowVisitor
    {
        private static FilenameAnalyzer _instance;  // For Singleton pattern

        /// <summary>
        /// Returns the sole instance of FilenameAnalyzer, creating it if necessary. This is the interface for the Singleton pattern.
        /// </summary>
        /// <returns>The sole instance of FilenameAnalyzer</returns>
        public static FilenameAnalyzer GetInstance()
        {
            if (_instance == null)
            {
                _instance = new FilenameAnalyzer();
            }
            return _instance;
        }

        /// <summary>
        /// Analyzes the filename of an Episode and searches for series, season, and episode values.
        /// </summary>
        /// <param name="ep">The Episode to be analyzed.</param>
        public override void VisitEpisode(Episode ep)
        {
            SimpleAnalysis(ep);
        }

        /// <summary>
        /// Analyzes the name of a Directory and searches for series, season, and episode values at a maximum certainty of Strong.
        /// </summary>
        /// <param name="dir">The Directory to be analyzed.</param>
        public override void VisitDirectory(Directory dir)
        {
            SimpleAnalysis(dir, Certainty.Strong);
        }

        /// <summary>
        /// Analyzes the supplied ShowNode's filename for series, season, and episode values. Optionally places an upper limit on certainty values.
        /// </summary>
        /// <param name="item">The ShowNode to be analyzed.</param>
        /// <param name="maxCertainty">The maximum certainty of any detected values; default is Certainty.Complete.</param>
        private void SimpleAnalysis(ShowNode item, Certainty maxCertainty = Certainty.Complete)
        {
            // Set up working variables
            string seriesCandidate;
            int seasonCandidate;
            int episodeCandidate;
            Certainty result;

            // Try to find the series name by searching for the "s01e01"-type pattern
            result = FindSeriesByPattern(item.Filename, out seriesCandidate);
            if (item is Directory) result++;
            if (result > maxCertainty) result = maxCertainty;
            item.AddSeriesCandidate(seriesCandidate, result);

            // If we aren't sure, also try using keywords
            if (result < Certainty.Complete)
            {
                result = FindSeriesByKeyword(item.Filename, out seriesCandidate);
                if (item is Directory) result++;
                if (result > maxCertainty) result = maxCertainty;
                item.AddSeriesCandidate(seriesCandidate, result);
            }

            // Try to find season and episode by searching for the "s01e01"-type pattern
            result = FindSeasonAndEpisode(item.Filename, out seasonCandidate, out episodeCandidate);
            if (item is Directory) result++;
            if (result > maxCertainty) result = maxCertainty;
            item.SetSeason(seasonCandidate, result);
            item.SetEpisode(episodeCandidate, Certainty.Complete);

            // If that didn't work, try to find the season using keywords
            if (result < Certainty.Complete)
            {
                result = FindSeasonByKeyword(item.Filename, out seasonCandidate);
                if (item is Directory) result++;
                if (result > maxCertainty) result = maxCertainty;
                item.SetSeason(seasonCandidate, result);
            }
        }

        /// <summary>
        /// Finds a "s01e01"-type pattern in a given filename and attempts to return a series name candidate by proximity to it.
        /// </summary>
        /// <param name="filename">The filename to be analyzed.</param>
        /// <param name="seriesCandidate">The detected series name.</param>
        /// <returns>The certainty of the detected series name.</returns>
        private Certainty FindSeriesByPattern(string filename, out string seriesCandidate)
        {
            // The series name is usually the text preceeding the season/episode identifier
            string[] dividedFilename = Regex.Split(filename, _episodeIdentifierPattern);
            if (dividedFilename.Length > 1)
            {
                string workingSeriesName = dividedFilename[0];

                // Replace periods and underscores with spaces
                workingSeriesName = workingSeriesName.Replace('.', ' ');
                workingSeriesName = workingSeriesName.Replace('_', ' ');
                workingSeriesName = workingSeriesName.Trim(new char[] { ' ', '-' });

                // Check that we have more than just spaces
                if (string.IsNullOrEmpty(workingSeriesName))
                {
                    seriesCandidate = string.Empty;
                    return Certainty.None;
                }

                // Fix capitalization
                workingSeriesName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(workingSeriesName);
                seriesCandidate = workingSeriesName;
                return Certainty.Strong;
            }

            seriesCandidate = string.Empty;
            return Certainty.None;
        }

        /// <summary>
        /// Searches a filename for a keyword and determines a likely series on that basis. Fails if the filename is simply a season identifier, and defaults to the filename if no keywords are found.
        /// </summary>
        /// <param name="filename">The filename to be analyzed.</param>
        /// <param name="seriesCandidate">The detected series name.</param>
        /// <returns>The certainty of the detected series name.</returns>
        private Certainty FindSeriesByKeyword(string filename, out string seriesCandidate)
        {
            // If the folder is simply called something to the effect of "season X", fail and allow the higher-level folder to take precendence
            foreach (string keyword in seasonKeywords)
            {
                Match theMatch = Regex.Match(filename, keyword, RegexOptions.IgnoreCase);
                if (theMatch.Success && theMatch.Value == filename)
                {
                    seriesCandidate = string.Empty;
                    return Certainty.None;
                }
            }

            // Try looking before the season identifier
            foreach (string keyword in seasonKeywords)
            {
                string[] separatedFolderName = Regex.Split(filename, keyword, RegexOptions.IgnoreCase);
                if (separatedFolderName.Count() > 1 && separatedFolderName[0] != string.Empty)
                {
                    seriesCandidate = separatedFolderName[0].Trim();
                    if (string.IsNullOrEmpty(seriesCandidate))
                    {
                        return Certainty.None;
                    }
                    return Certainty.Strong;
                }
            }

            // Default to using the simple formatted filename
            seriesCandidate = filename.Trim();
            return Certainty.None;
        }

        /// <summary>
        /// Finds the season by searching for keywords in the filename.
        /// </summary>
        /// <param name="filename">The filename to search.</param>
        /// <param name="seasonNumCandidate">The detected season number.</param>
        /// <returns>The certaitny of the detected season number.</returns>
        private Certainty FindSeasonByKeyword(string filename, out int seasonNumCandidate)
        {
            foreach (string keyword in seasonKeywords)
            {
                Match seasonMatch = Regex.Match(filename, keyword, RegexOptions.IgnoreCase);
                if (seasonMatch.Success)
                {
                    string seasonString = Regex.Match(seasonMatch.Value, @"\d+").Value;
                    if (int.TryParse(seasonString, out seasonNumCandidate))
                    {
                        return Certainty.Strong;
                    }
                }
            }

            seasonNumCandidate = -2;
            return Certainty.None;
        }

        /// <summary>
        /// Searches the filename to find season and episode values.
        /// </summary>
        /// <param name="filename">The filename to search.</param>
        /// <param name="seasonCandidate">The detected season number.</param>
        /// <param name="episodeCandidate">The detected episode number.</param>
        /// <returns>The certainty of the detected season and episode.</returns>
        private Certainty FindSeasonAndEpisode(string filename, out int seasonCandidate, out int episodeCandidate)
        {
            // ATTEMPT ONE: Try to find the "s01e01"-type pattern in the filename

            Match episodeIDMatch;
            int seasonNum = -2;
            int episodeNum = 0;
            try
            {
                episodeIDMatch = Regex.Match(filename, _episodeIdentifierPattern, RegexOptions.IgnoreCase);
            }
            catch
            {
                seasonCandidate = seasonNum;
                episodeCandidate = episodeNum;
                return Certainty.None;
            }

            if (episodeIDMatch.Success)
            {
                // Loop through each instance of the pattern; the first valid occurrence will be used
                foreach (Capture capture in episodeIDMatch.Captures)
                {
                    // Pull the digits from the capture and return their values
                    string captureStr = capture.Value.Replace('_', ' ');
                    captureStr = captureStr.Replace('-', ' ');
                    captureStr = captureStr.Replace(',', ' ');
                    captureStr = captureStr.Trim();
                    string[] numbersInCapture = Regex.Split(captureStr, @"[^\d]+");

                    // If the season is marked with an "S" (like "s02e04")
                    if (numbersInCapture.Length == 3 && int.TryParse(numbersInCapture[1], out seasonNum) && int.TryParse(numbersInCapture[2], out episodeNum))
                    {
                        seasonCandidate = seasonNum;
                        episodeCandidate = episodeNum;
                        return Certainty.Complete;
                    }

                    // If there is no marker for the season (like "2x04")
                    if (numbersInCapture.Length == 2 && int.TryParse(numbersInCapture[0], out seasonNum) && int.TryParse(numbersInCapture[1], out episodeNum))
                    {
                        seasonCandidate = seasonNum;
                        episodeCandidate = episodeNum;
                        return Certainty.Complete;
                    }
                }
            }

            // ATTEMPT TWO: Try to find identifying keywords

            foreach (string keyword in seasonKeywords)
            {
                episodeIDMatch = Regex.Match(filename, keyword, RegexOptions.IgnoreCase);
                if (episodeIDMatch.Success)
                {
                    string seasonString = Regex.Match(episodeIDMatch.Value, @"\d+").Value;
                    if (int.TryParse(seasonString, out seasonNum))
                    {
                        break;
                    }
                }
            }

            foreach (string keyword in episodeKeywords)
            {
                episodeIDMatch = Regex.Match(filename, keyword, RegexOptions.IgnoreCase);
                if (episodeIDMatch.Success)
                {
                    string episodeString = Regex.Match(episodeIDMatch.Value, @"\d+").Value;
                    if (int.TryParse(episodeString, out episodeNum))
                    {
                        break;
                    }
                }
            }

            // If attempt two succeeded for both season and episode, let's return it
            if (seasonNum != -2 && episodeNum != -1)
            {
                seasonCandidate = seasonNum;
                episodeCandidate = episodeNum;
                return Certainty.Strong;
            }

            // If only episode succeeded, let's fail now and try to find the season elsewhere
            if (seasonNum == -2 && episodeNum != -1)
            {
                seasonCandidate = seasonNum;
                episodeCandidate = episodeNum;
                return Certainty.Weak;
            }

            // ATTEMPT THREE: See if there are two numbers in the filename, and assume they're season and episode
            string[] numbersInFilenameRaw = Regex.Split(filename, @"[^\d]+");
            List<string> numbersFound = new List<string>();
            foreach (string number in numbersInFilenameRaw)
            {
                if (number != string.Empty)
                {
                    numbersFound.Add(number);
                }
            }

            if (numbersFound.Count == 2 && int.TryParse(numbersFound[0], out seasonNum) && int.TryParse(numbersFound[1], out episodeNum))
            {
                seasonCandidate = seasonNum;
                episodeCandidate = episodeNum;
                return Certainty.Weak;
            }

            // None of the above attempts have worked; return false
            seasonCandidate = seasonNum;
            episodeCandidate = episodeNum;
            return Certainty.None;
        }
    }
}
