﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Xml;
using System.Text.RegularExpressions;

namespace Batru
{
    /// <summary>
    /// Static class that creates, maintains, manipulates, and provides access to Series objects.
    /// </summary>
    public static class SeriesFactory
    {
        public static bool OfflineMode = false;
        public static List<Series> _allSeries = new List<Series>();
        private static Dictionary<string, Series> _aliases = new Dictionary<string, Series>();
        public static int downloadedSeries = 0;

        /// <summary>
        /// Returns an instance of Series with the given title, creating it if necessary.
        /// </summary>
        /// <param name="title">The title of the Series to return.</param>
        /// <returns>A Series instance with the given title.</returns>
        public static Series GetSeries(string title)
        {
            if (string.IsNullOrEmpty(title))
            {
                return null;
            }

            // Try to locate an already-existing instance
            foreach (Series thisSeries in _allSeries)
            {
                if (CompareTitlesWithoutThe(title, thisSeries.Title))
                {
                    return thisSeries;
                }
            }

            // Check for aliases
            if (_aliases.ContainsKey(title.ToLowerInvariant()))
            {
                return _aliases[title.ToLowerInvariant()];
            }

            // Create a new instance if one does not already exist
            Series seriesToReturn = new Series();
            seriesToReturn.Title = title;
            _allSeries.Add(seriesToReturn);
            return seriesToReturn;
        }

        /// <summary>
        /// Returns an instance of Series with the given ID, creating it if necessary.
        /// </summary>
        /// <param name="id">The ID of the Series to return.</param>
        /// <returns>A Series instance with the given ID.</returns>
        public static Series GetSeries(int id, string title = "")
        {
            // Try to locate an already-existing instance
            foreach (Series thisSeries in _allSeries)
            {
                if (thisSeries.Id == id)
                {
                    return thisSeries;
                }
            }

            // Create a new instance if one does not already exist
            Series seriesToReturn = new Series();
            seriesToReturn.Id = id;
            if (title != "") seriesToReturn.Title = title;
            _allSeries.Add(seriesToReturn);
            return seriesToReturn;
        }

        /// <summary>
        /// Searches TheTVDB.com for a given series and returns all results in a Dictionary along with their TVDB id.
        /// </summary>
        /// <param name="showName">The show name to search for.</param>
        /// <returns>A Dictionary containing all shows found in the search, with their ID.</returns>
        public static List<Series> SearchForShow(Series theSeries)
        {
            if (OfflineMode)
            {
                return null;
            }

            List<Series> results = new List<Series>();
            XmlReader reader = null;
            reader = XmlReader.Create(@"http://thetvdb.com/api/GetSeries.php?seriesname=" + theSeries.Title);

            // Will be used to store search results temporarily before creating records
            int id = 0;
            string title = string.Empty;
            string[] aliases = null;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    if (reader.Name == "Series" && CompareTitles(title, theSeries.Title))    // If the Series tag is closing, get a Series reference and add it to the results
                    {
                        Series seriesToAdd = GetSeries(theSeries.Title);
                        seriesToAdd.Title = title;
                        seriesToAdd.Id = id;
                        if (aliases != null)
                        {
                            foreach (string alias in aliases)
                            {
                                if (!_aliases.ContainsKey(alias))
                                {
                                    _aliases.Add(alias, seriesToAdd);
                                }
                            }
                            aliases = null;
                        }
                        results.Add(seriesToAdd);
                    }
                }
                else if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "seriesid":        // The series ID of the result
                            id = reader.ReadElementContentAsInt();
                            break;
                        case "SeriesName":      // The series name of the result
                            title = reader.ReadElementContentAsString();
                            //title = FormatTitle(title);
                            break;
                        case "AliasNames":      // Alias names for the result
                            string rawAliases = reader.ReadElementContentAsString();
                            aliases = rawAliases.Split('|');
                            break;
                    }
                }
            }

            return results;
        }

        /// <summary>
        /// Given a Series with an ID, downloads its details from TheTVDB, alters them using the Series interface, and returns whether the Series now has a title and at least one season.
        /// </summary>
        /// <param name="seriesToDownload">The Series to download further details for.</param>
        /// <returns>Whether seriesToDownload now has a title and at least one season - ie, whether the check was successful.</returns>
        public static bool DownloadSeries(Series seriesToDownload)
        {
            if (OfflineMode)
            {
                return false;
            }

            XmlReader reader;
            try
            {
                reader = XmlReader.Create(@"http://thetvdb.com/api/84CADC33F2CD4B35/series/" + seriesToDownload.Id.ToString() + @"/all");
            }
            catch (WebException)
            {
                return false;
            }
            
            // Initialize variables that will be used to store show details
            int season = 0;
            int episode = 0;
            string name = string.Empty;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    // If we have reached the end of an Episode record, add that episode's details to showToReturn
                    if (reader.Name == "Episode")
                    {
                        seriesToDownload.AddEpisode(season, episode, name);
                    }
                    continue; // Short-circuit further processing on an EndElement
                }
                if (reader.NodeType == XmlNodeType.Element)
                {
                    switch (reader.Name)
                    {
                        case "SeriesName":  // Contains the series' name
                            seriesToDownload.Title = reader.ReadElementContentAsString();
                            break;
                        case "EpisodeName": // Contains the episode's name
                            name = reader.ReadElementContentAsString();
                            break;
                        case "EpisodeNumber":   // Contains the number of the episode within the season
                            episode = reader.ReadElementContentAsInt();
                            break;
                        case "SeasonNumber":    // Contains the number of the season this episode is a part of
                            season = reader.ReadElementContentAsInt();
                            break;
                    }
                }
            }

            if (seriesToDownload.Title != string.Empty && seriesToDownload.Seasons > 0)
            {
                seriesToDownload.Verified = true;
                downloadedSeries++;
            }
            return seriesToDownload.Verified;
        }

        /// <summary>
        /// Compares two titles to see if they are the same when year suffix, non-alphanumeric characters, the prefix "the ", and casing are ignored. Tests for abbreviations if the above tests fail.
        /// </summary>
        /// <param name="test">The candidate title to compare to an existing sample</param>
        /// <param name="control">The control title to compare to a new candidate.</param>
        /// <returns>Whether the two titles are equivalent.</returns>
        public static bool CompareTitles(string test, string control)
        {
            string formattedTest = test;
            string formattedControl = control;

            // Remove year suffix
            Regex reg = new Regex(@"\(\d\d\d\d\)$");
            if (test.Length > 6) formattedTest = reg.Replace(formattedTest, string.Empty);
            if (control.Length > 6) formattedControl = reg.Replace(formattedControl, string.Empty);

            // Remove non-alphanumeric characters
            reg = new Regex(@"[^A-Za-z\d ]");
            formattedTest = reg.Replace(formattedTest, string.Empty);
            formattedControl = reg.Replace(formattedControl, string.Empty);

            // Remove multiple spaces
            reg = new Regex(@"  +");
            formattedTest = reg.Replace(formattedTest, " ");
            formattedControl = reg.Replace(formattedControl, " ");

            // Check for abbreviations
            MatchCollection matches = GetAbbreviations(formattedTest);
            foreach (Match thisMatch in matches)
            {
                int precedingChars = thisMatch.Index;
                int followingChars = formattedTest.Length - thisMatch.Index - thisMatch.Length;
                if (formattedControl.Length < precedingChars + followingChars + 2)
                {
                    continue;
                }

                if (string.Equals(formattedTest.Substring(0, precedingChars), formattedControl.Substring(0, precedingChars))
                    && string.Equals(formattedTest.Substring(precedingChars), formattedControl.Substring(precedingChars)))
                {
                    string abbreviation = thisMatch.Value;
                    string expanded = formattedControl.Substring(precedingChars, formattedControl.Length - precedingChars - followingChars);
                    for (int c = 0; c < expanded.Length; c++)
                    {
                        if (expanded[c] == abbreviation[0])
                        {
                            if (abbreviation.Length > 1)
                            {
                                abbreviation = abbreviation.Substring(1);
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            // Remove "The"
            if (formattedTest.Length > 4 && string.Equals(formattedTest.Substring(0, 4), "the ", StringComparison.OrdinalIgnoreCase))
            {
                formattedTest = formattedTest.Substring(4);
            }
            if (formattedControl.Length > 4 && string.Equals(formattedControl.Substring(0, 4), "the ", StringComparison.OrdinalIgnoreCase))
            {
                formattedControl = formattedControl.Substring(4);
            }

            formattedTest = formattedTest.Trim();
            formattedControl = formattedControl.Trim();

            // Succeed if they are equivalent
            if (string.Equals(formattedTest, formattedControl, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Extracts potential abbreviations from a given string.
        /// </summary>
        /// <param name="input">The string to search for abbreviations.</param>
        /// <returns>A Regex.MatchCollection of potential abbreviations found.</returns>
        public static MatchCollection GetAbbreviations(string input)
        {
            Regex reg = new Regex(@"\b[A-Z0-9][A-Z0-9]*\b");
            return reg.Matches(input);
        }

        /// <summary>
        /// Returns whether two given strings are the same, ignoring case and the word "the " at the beginning.
        /// </summary>
        /// <param name="test">The test string to check for a match with control.</param>
        /// <param name="control">The control string to check for a match with test.</param>
        /// <returns>Whether the two supplied strings are a match.</returns>
        public static bool CompareTitlesWithoutThe(string test, string control)
        {
            // Remove "The"
            if (test.Length > 4 && string.Equals(test.Substring(0, 4), "the ", StringComparison.OrdinalIgnoreCase))
            {
                test = test.Substring(4);
            }
            if (control.Length > 4 && string.Equals(control.Substring(0, 4), "the ", StringComparison.OrdinalIgnoreCase))
            {
                control = control.Substring(4);
            }

            test = test.Trim();
            control = control.Trim();

            if (control.Equals(test, StringComparison.OrdinalIgnoreCase))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Resets _allSeries and _aliases to empty collections.
        /// </summary>
        public static void ClearCache()
        {
            _allSeries = new List<Series>();
            _aliases = new Dictionary<string, Series>();
        }
    }
}
