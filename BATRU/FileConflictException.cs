﻿/* Copyright (C)2014
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of 
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. 
 * If not, see https://www.gnu.org/copyleft/gpl.html.
 * 
 * You can contact the developer of Bulk Automatic TV Rename Utility, David Proctor, at
 * david@davidproctor.ca.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Batru
{
    /// <summary>
    /// An exception that adds detail to IOException when trying to move or copy a file to an occupied name.
    /// </summary>
    public class FileConflictException : Exception
    {
        /// <summary>
        /// Constructor that attaches the filename and directory of the problem files to the exception.
        /// </summary>
        /// <param name="filename">The filename causing the conflict.</param>
        /// <param name="directory">The directory the conflict is located in.</param>
        public FileConflictException(string filename, string directory)
        {
            this.Filename = filename;
            this.Directory = directory;
        }

        /// <summary>
        /// The name of the file causing the conflict.
        /// </summary>
        public string Filename
        {
            get;
            set;
        }

        /// <summary>
        /// The directory the conflict is occurring in.
        /// </summary>
        public string Directory
        {
            get;
            set;
        }
    }
}
